export const LOGOUT = 'UNAUTHORIZED';

export const BUTTON_HIGHLIGHTS = {
  disable: 1,
};

export const INPUTS_MAX_VALUES = {
  firstName: 20,
  lastName: 30,
  middleName: 30,
  email: 50,
  phone: 15,
  country: 100,
};

export const PICK_MODAL_BUTTON_BORDER_RADIUS = {
  around: {
    borderRadius: 15,
  },
  top: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  bottom: {
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
  },
  none: {
    borderRadius: 0,
  },
};
