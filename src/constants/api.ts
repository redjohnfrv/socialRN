export const graphqlURI = 'https://internship-social-media.purrweb.com/graphql';
export const restAPI = 'https://internship-social-media.purrweb.com';
export const s3_signed_URL = '/v1/aws/signed-url';
export const s3_delete_file = '/v1/aws/delete-s3-file';
export const appUri = 'socialrnapp://';

/**
 Sign S3 URL example: https://internship-social-media.purrweb.com/v1/aws/signed-url?fileName=1.jpg&fileCategory=AVATARS
 */
