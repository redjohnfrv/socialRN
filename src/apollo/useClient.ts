import {ApolloClient, createHttpLink, InMemoryCache} from '@apollo/client';
import {useToken} from '../hooks/useToken';
import {graphqlURI} from '../constants/api';

export const useClient = () => {
  const {token, setToken} = useToken();

  const customFetch = (uri: string, options?: any) => {
    if (token) {
      options.headers.authorization = `Bearer ${token}`;
    }

    return fetch(uri, options).then(async response => {
      const json = await response.clone().json();
      const error = json?.errors?.[0];

      if (error && error.extensions.code === 'UNAUTHENTICATED') {
        setToken('');
      }

      return response;
    });
  };

  const httpLink = createHttpLink({
    uri: graphqlURI,
    fetch: customFetch,
  });

  const client = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache(),
  });

  return {
    client,
  };
};
