import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import Modal from 'react-native-modal';
import {PICK_MODAL_BUTTON_BORDER_RADIUS} from '../../constants/common';
import {Button, GroupButtons} from './components';

type Props = {
  isVisible: boolean;
  onClose: () => void;
  onImageLibraryPress: () => void;
  onCameraPress: () => void;
  onDeleteImage: () => void;
};

export const ImagePickerModal = (props: Props) => {
  const {
    isVisible,
    onClose,
    onImageLibraryPress,
    onCameraPress,
    onDeleteImage,
  } = props;

  return (
    <Modal
      isVisible={isVisible}
      statusBarTranslucent={true}
      onBackButtonPress={onClose}
      onBackdropPress={onClose}
      style={styles.modal}>
      <SafeAreaView>
        <GroupButtons
          onImageLibraryPress={onImageLibraryPress}
          onCameraPress={onCameraPress}
          onDeleteImage={onDeleteImage}
        />

        <Button
          title="Cancel"
          onPress={onClose}
          borderRadius={PICK_MODAL_BUTTON_BORDER_RADIUS.around}
        />
      </SafeAreaView>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 0,
    marginTop: 'auto',
  },
});
