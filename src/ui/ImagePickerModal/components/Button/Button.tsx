import React from 'react';
import {
  GestureResponderEvent,
  StyleProp,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableHighlightProps,
  ViewStyle,
} from 'react-native';
import {useSwitcher} from '../../../../hooks/useSwitcher';
import {typography} from '../../../../assets/styles/typography';
import {useThemeColors} from '../../../../hooks/useThemeColors';
import {colors} from '../../../../assets/styles/colors';

type Props = {
  title: string;
  customStyles?: StyleProp<ViewStyle>;
  isLoading?: boolean;
  borderRadius: Record<string, number>;
  isDeleteButton?: boolean;
} & TouchableHighlightProps;

export const Button = (props: Props) => {
  const {
    title,
    customStyles,
    isLoading,
    disabled,
    onPress,
    borderRadius,
    isDeleteButton = false,
    ...buttonProps
  } = props;
  const {
    isOn: isPress,
    on: setIsPressOn,
    off: setIsPressOff,
  } = useSwitcher(false);
  const {modeColor, modeColorInverse} = useThemeColors();
  const isDisabled = disabled || isLoading;

  const cs = typeof customStyles === 'object' ? customStyles : undefined;

  const pressTextStyles = isPress
    ? modeColorInverse.primary.default
    : modeColorInverse.primary.default;

  const textColor = isDeleteButton ? colors.additional.error : pressTextStyles;
  const bgColor = isPress ? 'black' : modeColor.grayscale[100];

  return (
    <TouchableHighlight
      disabled={isDisabled}
      style={[
        styles.button,
        {...cs, backgroundColor: bgColor, ...borderRadius},
      ]}
      onHideUnderlay={() => setIsPressOff()}
      onShowUnderlay={() => setIsPressOn()}
      onPress={(e: GestureResponderEvent) => onPress && onPress(e)}
      {...buttonProps}>
      <Text style={[styles.buttonText, {color: textColor}]}>{title}</Text>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    paddingVertical: 12,
  },
  buttonText: {
    ...typography.Body2_Medium_16pt,
  },
});
