import React from 'react';
import {StyleSheet, View} from 'react-native';
import {useThemeColors} from '../../../../hooks/useThemeColors';
import {PICK_MODAL_BUTTON_BORDER_RADIUS} from '../../../../constants/common';
import {Button} from '../Button';

type Props = {
  onImageLibraryPress: () => void;
  onCameraPress: () => void;
  onDeleteImage: () => void;
};

export const GroupButtons = (props: Props) => {
  const {onImageLibraryPress, onDeleteImage, onCameraPress} = props;

  const {modeColor} = useThemeColors();

  return (
    <View
      style={[
        styles.root,
        {backgroundColor: modeColor.grayscale[100], borderRadius: 15},
      ]}>
      <Button
        title="Take a photo"
        onPress={onCameraPress}
        borderRadius={PICK_MODAL_BUTTON_BORDER_RADIUS.top}
      />
      <View
        style={[styles.separator, {backgroundColor: modeColor.grayscale[200]}]}
      />
      <Button
        title="Choose from the library"
        onPress={onImageLibraryPress}
        borderRadius={PICK_MODAL_BUTTON_BORDER_RADIUS.none}
      />
      <View
        style={[styles.separator, {backgroundColor: modeColor.grayscale[200]}]}
      />
      <Button
        title="Delete photo"
        onPress={onDeleteImage}
        borderRadius={PICK_MODAL_BUTTON_BORDER_RADIUS.bottom}
        isDeleteButton={true}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    marginBottom: 8,
  },
  separator: {
    width: '100%',
    height: 0.5,
  },
});
