import React, {useCallback, useMemo} from 'react';
import {
  KeyboardType,
  NativeSyntheticEvent,
  StyleProp,
  StyleSheet,
  Text,
  TextInput,
  TextInputFocusEventData,
  TextInputProps,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import {useThemeColors} from '../../hooks/useThemeColors';
import {useSwitcher} from '../../hooks/useSwitcher';
import {typography} from '../../assets/styles/typography';
import {colors} from '../../assets/styles/colors';
import {returnVariant} from '../../utils';
import SuccessSvg from '../../assets/svg/success.svg';
import {EyeIcon} from '../ComponentsSvg';

type Props = {
  title?: string;
  success?: boolean;
  error?: string;
  customStyles?: StyleProp<ViewStyle>;
  enabled?: boolean;
  secure?: boolean;
  show?: boolean;
  toggleShow?: () => void;
  keyboardType?: KeyboardType;
} & TextInputProps;

export const DefaultInput = (props: Props) => {
  const {
    title,
    success = false,
    error = '',
    customStyles,
    enabled = true,
    secure = false,
    show = false,
    toggleShow,
    onBlur,
    onFocus,
    value,
    keyboardType = 'default',
    multiline,
    ...inputProps
  } = props;
  const {modeColor} = useThemeColors();
  const {
    isOn: isFocusedOn,
    on: setFocusOn,
    off: setFocusOff,
  } = useSwitcher(false);

  const isInputFilled = isFocusedOn || !!value;

  const inputStyles = useMemo(
    () => ({
      color: returnVariant.getColorByState(
        modeColor.grayscale[700],
        success,
        error,
      ),
      borderBottomWidth: 1,
      borderBottomColor: returnVariant.getColorByState(
        isInputFilled ? modeColor.grayscale[700] : colors.dark.grayscale[250],
        success,
        error,
      ),
    }),
    [error, isInputFilled, modeColor.grayscale, success],
  );

  const getPlaceholderColor = useCallback(() => {
    if (error) {
      return colors.additional.error;
    } else {
      return isFocusedOn
        ? modeColor.grayscale[700]
        : colors.dark.grayscale[300];
    }
  }, [error, isFocusedOn, modeColor.grayscale]);

  const onBlurHandler = (e: NativeSyntheticEvent<TextInputFocusEventData>) => {
    onBlur && onBlur(e);
    setFocusOff();
  };

  const onFocusHandler = (e: NativeSyntheticEvent<TextInputFocusEventData>) => {
    onFocus && onFocus(e);
    setFocusOn();
  };

  return (
    <View style={customStyles}>
      {title && <Text style={styles.title}>{title}</Text>}

      <TextInput
        {...inputProps}
        keyboardType={keyboardType}
        secureTextEntry={!show && secure}
        onFocus={e => onFocusHandler(e)}
        onBlur={e => onBlurHandler(e)}
        style={[inputStyles, styles.default]}
        placeholderTextColor={getPlaceholderColor()}
        editable={enabled}
        selectTextOnFocus={enabled}
        value={value}
        multiline={multiline}
      />

      {success && !secure && (
        <View style={styles.icon}>
          <SuccessSvg width={14} height={12} />
        </View>
      )}

      {error && (
        <Text numberOfLines={1} style={styles.error}>
          {error}
        </Text>
      )}

      {secure && (
        <TouchableOpacity
          style={[{top: !!error ? '40%' : '50%'}, styles.icon]}
          onPress={toggleShow}>
          <EyeIcon
            show={show}
            variant={returnVariant.getEyeSecureInputVariant(
              !!error,
              success,
              isInputFilled,
            )}
          />
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    position: 'relative',
    flexDirection: 'column',
  },
  title: {
    color: colors.light.grayscale[300],
    ...typography.Headline_2_SemiBold_14pt,
  },
  default: {
    paddingLeft: 0,
    paddingRight: 20,
    backgroundColor: 'transparent',
    ...typography.Body5_Regular_16pt,
  },
  icon: {
    position: 'absolute',
    right: 0,
  },
  error: {
    marginTop: 4,
    color: colors.additional.error,
    ...typography.Body6_Regular_14pt,
  },
});
