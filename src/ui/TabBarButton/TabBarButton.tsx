import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableOpacityProps,
  View,
} from 'react-native';
import {useThemeColors} from '../../hooks/useThemeColors';
import {BUTTON_HIGHLIGHTS} from '../../constants/common';
import {typography} from '../../assets/styles/typography';

type Props = {
  title: string;
  active: boolean;
  Icon: () => JSX.Element;
} & TouchableOpacityProps;

export const TabBarButton = (props: Props) => {
  const {title, active, Icon, ...buttonProps} = props;
  const {modeColor, modeColorInverse} = useThemeColors();

  return (
    <TouchableOpacity
      activeOpacity={BUTTON_HIGHLIGHTS.disable}
      {...buttonProps}>
      <View style={styles.wrapper}>
        {Icon()}

        <Text
          style={[
            styles.title,
            {
              color: active
                ? modeColorInverse.primary.default
                : modeColor.grayscale[250],
            },
          ]}>
          {title}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {},
  wrapper: {
    alignItems: 'center',
  },
  title: {
    marginTop: 4,
    ...typography.Caption1_Regular_12pt,
  },
});
