import React, {useContext} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {images} from '../../assets/images';
import {ThemeContext} from '../../context/ThemeContext';

type ImageSizeVariant = 'large' | 'medium' | 'small';

type Props = {
  src: string | {uri: string};
  variant: ImageSizeVariant;
};

export const DefaultAvatar = (props: Props) => {
  const {src, variant} = props;
  const {isDarkMode} = useContext(ThemeContext);

  const defaultImage = isDarkMode ? images.avatarDark : images.avatarLight;
  const image = src ? src : defaultImage;

  const getImageSize = (propsVariant: ImageSizeVariant) => {
    if (propsVariant === 'large') {
      return {width: 160, height: 160, borderRadius: 80};
    }
    if (propsVariant === 'medium') {
      return {width: 80, height: 80, borderRadius: 40};
    }

    return {width: 40, height: 40, borderRadius: 20};
  };

  return (
    <View style={[styles.root, getImageSize(variant)]}>
      <Image
        style={[styles.image, getImageSize(variant)]}
        source={image}
        accessible
        accessibilityLabel="user avatar"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
  },
  image: {
    flex: 1,
    resizeMode: 'contain',
  },
});
