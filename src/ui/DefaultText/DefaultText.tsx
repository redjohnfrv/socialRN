import React from 'react';
import {Text, TextProps} from 'react-native';
import {useThemeColors} from '../../hooks/useThemeColors';
import {typography} from '../../assets/styles/typography';

type Props = {
  text: string;
  font: string;
} & TextProps;

export const DefaultText = (props: Props) => {
  const {text, font, style, ...textProps} = props;
  const {modeColor} = useThemeColors();

  const textStyles = {
    ...typography[font],
    color: modeColor.grayscale[700],
  };

  return (
    <Text style={[style, props.style, textStyles]} {...textProps}>
      {text}
    </Text>
  );
};
