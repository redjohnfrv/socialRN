import React from 'react';
import {Noop} from 'react-hook-form';
import {StyleProp, View, ViewStyle} from 'react-native';
import {useSwitcher} from '../../hooks/useSwitcher';
import {DefaultInput} from '../DefaultInput';

type Props = {
  title?: string;
  placeholder: string;
  customStyles?: StyleProp<ViewStyle>;
  success?: boolean;
  error?: string;
  onBlur: Noop;
  value: string;
  onChangeText: (...event: any[]) => void;
  enabled?: boolean;
};

export const SecureInput = (props: Props) => {
  const {isOn: isInputShown, toggle: toggleInputShow} = useSwitcher(false);

  return (
    <View>
      <DefaultInput
        secure={true}
        show={isInputShown}
        toggleShow={toggleInputShow}
        {...props}
      />
    </View>
  );
};
