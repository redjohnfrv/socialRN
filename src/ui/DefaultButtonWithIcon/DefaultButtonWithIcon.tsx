import React from 'react';
import {
  GestureResponderEvent,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableHighlightProps,
  View,
} from 'react-native';
import {useSwitcher} from '../../hooks/useSwitcher';
import {useThemeColors} from '../../hooks/useThemeColors';
import {MenuIconVariant} from '../../types';
import {typography} from '../../assets/styles/typography';
import {ExitIcon, ThemeIcon, UserIcon} from '../ComponentsSvg';

type Props = {
  text: string;
  iconVariant: MenuIconVariant;
} & TouchableHighlightProps;

export const DefaultButtonWithIcon = (props: Props) => {
  const {text, iconVariant, onPress, disabled = false} = props;
  const {modeColor, modeColorInverse} = useThemeColors();
  const {
    isOn: isPress,
    on: setIsPressOn,
    off: setIsPressOff,
  } = useSwitcher(false);

  const disabledColorStyles = {
    color: modeColor.grayscale[250],
  };

  const defaultColorStyles = {
    color: isPress
      ? modeColorInverse.primary.default
      : modeColor.grayscale[700],
  };

  const colorStyles = disabled ? disabledColorStyles : defaultColorStyles;

  const getIcon = (variant: MenuIconVariant) => {
    if (variant === 'profile') {
      return <UserIcon active={isPress} disable={disabled} />;
    }
    if (variant === 'exit') {
      return <ExitIcon active={isPress} disable={disabled} />;
    }
    if (variant === 'mode') {
      return <ThemeIcon active={isPress} disable={disabled} />;
    }
  };

  return (
    <TouchableHighlight
      underlayColor={'transparent'}
      disabled={disabled}
      onHideUnderlay={() => setIsPressOff()}
      onShowUnderlay={() => setIsPressOn()}
      onPress={(e: GestureResponderEvent) => onPress && onPress(e)}>
      <View style={styles.root}>
        {getIcon(iconVariant)}
        <Text style={[styles.text, {...colorStyles}]}>{text}</Text>
      </View>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    alignItems: 'center',
  },
  text: {
    marginLeft: 8,
    ...typography.Body4_Regular_18pt,
  },
});
