import React, {useMemo} from 'react';
import {
  GestureResponderEvent,
  StyleProp,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableHighlightProps,
  ViewStyle,
} from 'react-native';
import {useThemeColors} from '../../hooks/useThemeColors';
import {useSwitcher} from '../../hooks/useSwitcher';
import {typography} from '../../assets/styles/typography';
import {colors} from '../../assets/styles/colors';

type Props = {
  title: string;
  isLoading?: boolean;
  customStyles?: StyleProp<ViewStyle>;
} & TouchableHighlightProps;

export const DefaultTextButton = (props: Props) => {
  const {title, disabled, isLoading, customStyles, onPress, ...buttonProps} =
    props;
  const {modeColorInverse} = useThemeColors();
  const {
    isOn: isPress,
    on: setIsPressOn,
    off: setIsPressOff,
  } = useSwitcher(false);
  const isDisabled = disabled || isLoading;
  const cs = typeof customStyles === 'object' ? customStyles : undefined;

  const buttonDisableStyles = {
    borderBottomColor: colors.light.grayscale[400],
    color: colors.light.grayscale[400],
  };

  const buttonDefaultStyles = useMemo(
    () => ({
      borderBottomColor: isPress
        ? colors.light.primary.pressed
        : modeColorInverse.primary.default,
      color: isPress
        ? colors.light.primary.pressed
        : modeColorInverse.primary.default,
    }),
    [isPress, modeColorInverse.primary.default],
  );

  const buttonStateStyles = isDisabled
    ? buttonDisableStyles
    : buttonDefaultStyles;

  return (
    <TouchableHighlight
      disabled={isDisabled}
      underlayColor={'transparent'}
      style={[
        {borderBottomColor: buttonStateStyles.borderBottomColor},
        styles.button,
        {...cs},
      ]}
      onHideUnderlay={() => setIsPressOff()}
      onShowUnderlay={() => setIsPressOn()}
      onPress={(e: GestureResponderEvent) => onPress && onPress(e)}
      {...buttonProps}>
      <Text style={[{color: buttonStateStyles.color}, styles.buttonText]}>
        {title}
      </Text>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  button: {
    position: 'relative',
    alignSelf: 'flex-start',
    flexDirection: 'column',
    backgroundColor: 'transparent',
    borderBottomWidth: 1,
  },
  buttonText: {
    ...typography.Body2_Medium_16pt,
    lineHeight: 26,
  },
});
