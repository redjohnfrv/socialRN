import React from 'react';
import {
  StyleProp,
  Text,
  TouchableHighlight,
  TouchableHighlightProps,
  ViewStyle,
} from 'react-native';
import {useButtonStyles} from '../../hooks/useButtonStyles';
import {LoaderIcon} from '../ComponentsSvg';
import {AnimatedRotate} from '../Animated';
import {typography} from '../../assets/styles/typography';

export type ButtonType = 'large' | 'medium' | 'small' | 'textIcon';

type Props = {
  title: string;
  type: ButtonType;
  customStyles?: StyleProp<ViewStyle>;
  isLoading?: boolean;
} & TouchableHighlightProps;

export const DefaultButton = (props: Props) => {
  const {
    title,
    type,
    onPress,
    customStyles,
    disabled,
    isLoading = false,
    ...buttonProps
  } = props;

  const isDisabled = disabled || isLoading;

  const {
    isPress,
    touchProps,
    pressedButtonStyles,
    initialButtonStyles,
    disabledButtonStyles,
  } = useButtonStyles(type, onPress, customStyles, isDisabled);

  const pressedStyles = isPress
    ? pressedButtonStyles.color
    : initialButtonStyles.color;

  const colorStyles = isDisabled ? disabledButtonStyles.color : pressedStyles;

  return (
    <TouchableHighlight {...touchProps} {...buttonProps} disabled={isDisabled}>
      {isLoading ? (
        <AnimatedRotate>
          <LoaderIcon />
        </AnimatedRotate>
      ) : (
        <Text style={[{color: colorStyles, ...typography.Body2_Medium_16pt}]}>
          {title}
        </Text>
      )}
    </TouchableHighlight>
  );
};
