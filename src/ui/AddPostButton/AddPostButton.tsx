import React from 'react';
import {
  GestureResponderEvent,
  TouchableHighlight,
  TouchableHighlightProps,
} from 'react-native';
import {useSwitcher} from '../../hooks/useSwitcher';
import {PlusIcon} from '../ComponentsSvg';

type Props = {} & TouchableHighlightProps;

export const AddPostButton = (props: Props) => {
  const {disabled, onPress} = props;
  const {
    isOn: isPress,
    on: setIsPressOn,
    off: setIsPressOff,
  } = useSwitcher(false);

  return (
    <TouchableHighlight
      underlayColor={'transparent'}
      disabled={disabled}
      onHideUnderlay={() => setIsPressOff()}
      onShowUnderlay={() => setIsPressOn()}
      onPress={(e: GestureResponderEvent) => onPress && onPress(e)}>
      <PlusIcon active={isPress} disable={disabled} />
    </TouchableHighlight>
  );
};
