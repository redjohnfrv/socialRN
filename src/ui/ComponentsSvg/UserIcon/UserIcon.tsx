import React from 'react';
import UserWhite from '../../../assets/svg/userWhite.svg';
import UserDark from '../../../assets/svg/userDark.svg';
import UserLightGreen from '../../../assets/svg/userLightGreen.svg';
import UserGreen from '../../../assets/svg/userGreen.svg';
import UserLightGray from '../../../assets/svg/userLightGray.svg';
import UserGray from '../../../assets/svg/userGray.svg';
import {SvgHoc} from '../_SvgHoc';

type Props = {
  active?: boolean;
  disable?: boolean;
};

export const UserIcon = (props: Props) => {
  const {active = false, disable = false} = props;

  return (
    <SvgHoc
      active={active}
      disable={disable}
      width={19.5}
      height={19.5}
      icons={{
        LightInitial: UserWhite,
        LightDisable: UserGray,
        DarkDisable: UserLightGray,
        DarkPressed: UserLightGreen,
        DarkInitial: UserDark,
        LightPressed: UserGreen,
      }}
    />
  );
};
