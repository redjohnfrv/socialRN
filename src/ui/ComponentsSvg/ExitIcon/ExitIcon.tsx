import React from 'react';
import ExitWhite from '../../../assets/svg/exitWhite.svg';
import ExitDark from '../../../assets/svg/exitDark.svg';
import ExitLightGreen from '../../../assets/svg/exitLightGreen.svg';
import ExitGreen from '../../../assets/svg/exitGreen.svg';
import ExitLightGray from '../../../assets/svg/exitLightGray.svg';
import ExitGray from '../../../assets/svg/exitGray.svg';
import {SvgHoc} from '../_SvgHoc';

type Props = {
  active?: boolean;
  disable?: boolean;
};

export const ExitIcon = (props: Props) => {
  const {active = false, disable = false} = props;
  return (
    <SvgHoc
      active={active}
      disable={disable}
      width={19.5}
      height={19.5}
      icons={{
        LightInitial: ExitWhite, //light initial
        LightDisable: ExitGray, //light disable
        DarkDisable: ExitLightGray, //dark disable
        DarkPressed: ExitLightGreen, //dark pressed
        DarkInitial: ExitDark, //dark initial
        LightPressed: ExitGreen, //light pressed
      }}
    />
  );
};
