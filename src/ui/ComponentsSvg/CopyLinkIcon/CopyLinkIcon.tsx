import React from 'react';
import LoaderWhiteSvg from '../../../assets/svg/copyLink.svg';
import LoaderBlackSvg from '../../../assets/svg/copyLinkBlack.svg';
import LoaderGraySvg from '../../../assets/svg/copyLinkGray.svg';
import {CopyLinkButtonVariant} from '../../../types';

type Props = {
  height?: number;
  width?: number;
  variant: CopyLinkButtonVariant;
};

export const CopyLinkIcon = (props: Props) => {
  const {height = 21, width = 17, variant} = props;

  if (variant === 'white') {
    return <LoaderWhiteSvg height={height} width={width} />;
  } else if (variant === 'black') {
    return <LoaderBlackSvg height={height} width={width} />;
  }

  return <LoaderGraySvg height={height} width={width} />;
};
