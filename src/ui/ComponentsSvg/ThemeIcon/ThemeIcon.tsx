import React from 'react';
import MoonWhite from '../../../assets/svg/moonWhite.svg';
import SunDark from '../../../assets/svg/sunDark.svg';
import MoonGreen from '../../../assets/svg/moonGreen.svg';
import SunGreen from '../../../assets/svg/sunGreen.svg';
import MoonGray from '../../../assets/svg/moonGray.svg';
import SunGray from '../../../assets/svg/sunGray.svg';
import {SvgHoc} from '../_SvgHoc';

type Props = {
  active?: boolean;
  disable?: boolean;
};

export const ThemeIcon = (props: Props) => {
  const {active = false, disable = false} = props;

  return (
    <SvgHoc
      active={active}
      disable={disable}
      width={19.5}
      height={19.5}
      icons={{
        LightInitial: MoonWhite,
        LightDisable: MoonGray,
        DarkDisable: SunGray,
        DarkPressed: MoonGreen,
        DarkInitial: SunDark,
        LightPressed: SunGreen,
      }}
    />
  );
};
