import React from 'react';
import PlusLightInitial from '../../../assets/svg/plusLightInitial.svg';
import PlusLightPressed from '../../../assets/svg/plusLightPressed.svg';
import PlusLightDisabled from '../../../assets/svg/plusLightDisabled.svg';
import PlusDarkInitial from '../../../assets/svg/plusDarkInitial.svg';
import PlusDarkPressed from '../../../assets/svg/plusDarkPressed.svg';
import PlusDarkDisabled from '../../../assets/svg/plusDarkDisabled.svg';
import {SvgHoc} from '../_SvgHoc';

type Props = {
  active?: boolean;
  disable?: boolean;
};

export const PlusIcon = (props: Props) => {
  const {active = false, disable = false} = props;
  return (
    <SvgHoc
      active={active}
      disable={disable}
      width={56}
      height={56}
      icons={{
        LightInitial: PlusLightInitial, //light initial
        LightPressed: PlusLightPressed, //light pressed
        LightDisable: PlusLightDisabled, //light disable
        DarkInitial: PlusDarkInitial, //dark initial
        DarkPressed: PlusDarkPressed, //dark pressed
        DarkDisable: PlusDarkDisabled, //dark disable
      }}
    />
  );
};
