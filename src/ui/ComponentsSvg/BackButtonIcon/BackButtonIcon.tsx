import React from 'react';
import ArrowWhite from '../../../assets/svg/arrowWhite.svg';
import ArrowDark from '../../../assets/svg/arrowDark.svg';
import ArrowGreen from '../../../assets/svg/arrowGreen.svg';
import ArrowGray from '../../../assets/svg/arrowGray.svg';
import ArrowLightGreen from '../../../assets/svg/arrowLightGreen.svg';
import ArrowLightGray from '../../../assets/svg/arrowLightGray.svg';
import {SvgHoc} from '../_SvgHoc';

type Props = {
  active?: boolean;
  disable?: boolean;
};

export const BackButtonIcon = (props: Props) => {
  const {active = false, disable = false} = props;

  return (
    <SvgHoc
      active={active}
      disable={disable}
      width={19.5}
      height={16.5}
      icons={{
        LightInitial: ArrowWhite,
        LightDisable: ArrowGray,
        DarkDisable: ArrowLightGray,
        DarkPressed: ArrowLightGreen,
        DarkInitial: ArrowDark,
        LightPressed: ArrowGreen,
      }}
    />
  );
};
