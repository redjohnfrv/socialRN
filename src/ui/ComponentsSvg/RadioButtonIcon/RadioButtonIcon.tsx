import React, {useContext} from 'react';
import {ThemeContext} from '../../../context/ThemeContext';
import DarkChecked from '../../../assets/svg/radioDarkChecked.svg';
import DarkUnchecked from '../../../assets/svg/radioDarkUnchecked.svg';
import LightChecked from '../../../assets/svg/radioLightChecked.svg';
import LightUnchecked from '../../../assets/svg/radioLightUnchecked.svg';

type Props = {
  checked: boolean;
};

export const RadioButtonIcon = (props: Props) => {
  const {checked} = props;
  const {isDarkMode} = useContext(ThemeContext);

  if (isDarkMode) {
    return checked ? (
      <DarkChecked height={20} width={20} />
    ) : (
      <DarkUnchecked height={20} width={20} />
    );
  }

  return checked ? (
    <LightChecked height={20} width={20} />
  ) : (
    <LightUnchecked height={20} width={20} />
  );
};
