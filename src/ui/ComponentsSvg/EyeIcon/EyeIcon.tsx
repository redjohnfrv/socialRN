import React, {useContext} from 'react';
import {ThemeContext} from '../../../context/ThemeContext';
import EyeClosedUnable from '../../../assets/svg/eyeClosedUnable.svg';
import EyeOpenUnable from '../../../assets/svg/eyeOpenUnable.svg';
import EyeClosedActive from '../../../assets/svg/eyeClosedActive.svg';
import EyeOpenActive from '../../../assets/svg/eyeOpenActive.svg';
import EyeClosedSuccess from '../../../assets/svg/eyeClosedSuccess.svg';
import EyeOpenSuccess from '../../../assets/svg/eyeOpenSuccess.svg';
import EyeClosedError from '../../../assets/svg/eyeClosedError.svg';
import EyeOpenError from '../../../assets/svg/eyeOpenError.svg';
import {EyeVariant} from '../../../types';

type Props = {
  height?: number;
  width?: number;
  show?: boolean;
  variant?: EyeVariant;
};

export const EyeIcon = (props: Props) => {
  const {height = 16, width = 19, show = false, variant = 'unable'} = props;
  const {isDarkMode} = useContext(ThemeContext);

  if (show) {
    if (variant === 'unable') {
      return <EyeOpenUnable height={height} width={width} />;
    } else if (variant === 'active') {
      if (isDarkMode) {
        return <EyeOpenActive height={height} width={width} />;
      } else {
        return <EyeOpenUnable height={height} width={width} />;
      }
    } else if (variant === 'success') {
      return <EyeOpenSuccess height={height} width={width} />;
    } else if (variant === 'error') {
      return <EyeOpenError height={height} width={width} />;
    }
  } else {
    if (variant === 'active') {
      if (isDarkMode) {
        return <EyeClosedActive height={height} width={width} />;
      } else {
        return <EyeClosedUnable height={height} width={width} />;
      }
    } else if (variant === 'success') {
      return <EyeClosedSuccess height={height} width={width} />;
    } else if (variant === 'error') {
      return <EyeClosedError height={height} width={width} />;
    }
  }

  return <EyeClosedUnable height={height} width={width} />;
};
