import React, {useContext} from 'react';
import {ThemeContext} from '../../../context/ThemeContext';
import SharedLight from '../../../assets/svg/lightShared.svg';
import SharedDark from '../../../assets/svg/darkShared.svg';
import SharedLightGreen from '../../../assets/svg/lightGreenShared.svg';
import SharedDarkGreen from '../../../assets/svg/darkGreenShared.svg';

type Props = {
  active?: boolean;
};

export const SharedIcon = (props: Props) => {
  const {active} = props;
  const {isDarkMode} = useContext(ThemeContext);

  if (isDarkMode) {
    return active ? (
      <SharedLightGreen width={19.5} height={17.5} />
    ) : (
      <SharedLight width={19.5} height={17.5} />
    );
  } else {
    return active ? (
      <SharedDarkGreen width={19.5} height={17.5} />
    ) : (
      <SharedDark width={19.5} height={17.5} />
    );
  }
};
