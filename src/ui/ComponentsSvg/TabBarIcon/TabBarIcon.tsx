import React, {useContext} from 'react';
import {ThemeContext} from '../../../context/ThemeContext';
import MainTabIconGray from '../../../assets/svg/tabHomeGray.svg';
import MainTabIconLightGreen from '../../../assets/svg/tabHomeLightGreen.svg';
import FavoritesTabIconGray from '../../../assets/svg/tabFavoritesGray.svg';
import FavoritesTabIconLightGreen from '../../../assets/svg/tabFavoritesLightGreen.svg';
import PostsTabIconGray from '../../../assets/svg/tabPostsGray.svg';
import PostsTabIconLightGreen from '../../../assets/svg/tabPostsLightGreen.svg';
import MainTabIconGreen from '../../../assets/svg/tabHomeGreen.svg';
import MainTabIconLightGray from '../../../assets/svg/tabHomeLightGray.svg';
import FavoritesTabIconGreen from '../../../assets/svg/tabFavoritesGreen.svg';
import FavoritesTabIconLightGray from '../../../assets/svg/tabFavoritesLightGray.svg';
import PostsTabIconGreen from '../../../assets/svg/tabPostsGreen.svg';
import PostsTabIconLightGray from '../../../assets/svg/tabPostsLightGray.svg';
import {TabBarIconVariant} from '../../../types';

type Props = {
  active?: boolean;
  variant?: TabBarIconVariant;
  width?: number;
  height?: number;
};

export const TabBarIcon = (props: Props) => {
  const {
    active = false,
    variant = TabBarIconVariant.MAIN,
    width = 21,
    height = 20,
  } = props;
  const {isDarkMode} = useContext(ThemeContext);

  if (isDarkMode) {
    if (variant === TabBarIconVariant.MAIN) {
      return active ? (
        <MainTabIconLightGreen width={width} height={height} />
      ) : (
        <MainTabIconGray width={width} height={height} />
      );
    }

    if (variant === TabBarIconVariant.FAVORITES) {
      return active ? (
        <FavoritesTabIconLightGreen width={width} height={height} />
      ) : (
        <FavoritesTabIconGray width={width} height={height} />
      );
    }

    if (variant === TabBarIconVariant.POSTS) {
      return active ? (
        <PostsTabIconLightGreen width={width} height={height} />
      ) : (
        <PostsTabIconGray width={width} height={height} />
      );
    }
  } else {
    if (variant === TabBarIconVariant.MAIN) {
      return active ? (
        <MainTabIconGreen width={width} height={height} />
      ) : (
        <MainTabIconLightGray width={width} height={height} />
      );
    }

    if (variant === TabBarIconVariant.FAVORITES) {
      return active ? (
        <FavoritesTabIconGreen width={width} height={height} />
      ) : (
        <FavoritesTabIconLightGray width={width} height={height} />
      );
    }

    if (variant === TabBarIconVariant.POSTS) {
      return active ? (
        <PostsTabIconGreen width={width} height={height} />
      ) : (
        <PostsTabIconLightGray width={width} height={height} />
      );
    }
  }

  return <MainTabIconGray width={width} height={height} />;
};
