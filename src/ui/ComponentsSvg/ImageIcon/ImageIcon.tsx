import React, {useContext} from 'react';
import {ThemeContext} from '../../../context/ThemeContext';
import ImageLight from '../../../assets/svg/lightImage.svg';
import ImageDark from '../../../assets/svg/darkImage.svg';
import ImageLightGreen from '../../../assets/svg/lightGreenImage.svg';
import ImageDarkGreen from '../../../assets/svg/darkGreenImage.svg';

type Props = {
  active?: boolean;
};

export const ImageIcon = (props: Props) => {
  const {active} = props;
  const {isDarkMode} = useContext(ThemeContext);

  if (isDarkMode) {
    return active ? (
      <ImageLightGreen width={19.5} height={17.5} />
    ) : (
      <ImageLight width={19.5} height={17.5} />
    );
  } else {
    return active ? (
      <ImageDarkGreen width={19.5} height={17.5} />
    ) : (
      <ImageDark width={19.5} height={17.5} />
    );
  }
};
