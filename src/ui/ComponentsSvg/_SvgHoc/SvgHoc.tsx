import React, {ElementType, useContext} from 'react';
import {ThemeContext} from '../../../context/ThemeContext';

type IconsType = {
  DarkPressed: ElementType;
  DarkDisable: ElementType;
  DarkInitial: ElementType;
  LightDisable: ElementType;
  LightInitial: ElementType;
  LightPressed: ElementType;
};

type Props = {
  active?: boolean;
  disable?: boolean;
  width: number;
  height: number;
  icons: IconsType;
};

export const SvgHoc = (props: Props) => {
  const {active = false, disable = false, width, height, icons} = props;
  const {isDarkMode} = useContext(ThemeContext);
  const {
    LightDisable,
    LightInitial,
    LightPressed,
    DarkPressed,
    DarkDisable,
    DarkInitial,
  } = icons;

  if (isDarkMode) {
    if (disable) {
      return <LightDisable width={width} height={height} />;
    } else {
      return active ? (
        <DarkPressed width={width} height={height} />
      ) : (
        <LightInitial width={width} height={height} />
      );
    }
  } else {
    if (disable) {
      return <DarkDisable width={width} height={height} />;
    } else {
      return active ? (
        <LightPressed width={width} height={height} />
      ) : (
        <DarkInitial width={width} height={height} />
      );
    }
  }
};
