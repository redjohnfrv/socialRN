import React from 'react';
import ImageIconLightInitial from '../../../assets/svg/avatarInitialLight.svg';
import ImageIconLightPressed from '../../../assets/svg/avatarPressedLight.svg';
import ImageIconLightDisable from '../../../assets/svg/avatarDisableLight.svg';
import ImageIconDarkInitial from '../../../assets/svg/avatarInitialDark.svg';
import ImageIconDarkPressed from '../../../assets/svg/avatarPressedDark.svg';
import ImageIconDarkDisable from '../../../assets/svg/avatarDisableDark.svg';
import {SvgHoc} from '../_SvgHoc';

type Props = {
  active?: boolean;
  disable?: boolean;
};

export const AvatarUploadIcon = (props: Props) => {
  const {active = false, disable = false} = props;
  return (
    <SvgHoc
      active={active}
      disable={disable}
      width={38}
      height={38}
      icons={{
        LightInitial: ImageIconDarkInitial,
        LightDisable: ImageIconLightDisable,
        DarkDisable: ImageIconDarkDisable,
        DarkPressed: ImageIconDarkPressed,
        DarkInitial: ImageIconLightInitial,
        LightPressed: ImageIconLightPressed,
      }}
    />
  );
};
