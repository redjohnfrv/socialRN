import React, {useContext} from 'react';
import {ThemeContext} from '../../../context/ThemeContext';
import HeartLight from '../../../assets/svg/lightHeart.svg';
import HeartDark from '../../../assets/svg/darkHeart.svg';
import HeartLightGreen from '../../../assets/svg/lightGreenHeart.svg';
import HeartDarkGreen from '../../../assets/svg/darkGreenHeart.svg';

type Props = {
  active?: boolean;
};

export const HeartIcon = (props: Props) => {
  const {active} = props;
  const {isDarkMode} = useContext(ThemeContext);

  if (isDarkMode) {
    return active ? (
      <HeartLightGreen width={19.5} height={17.5} />
    ) : (
      <HeartLight width={19.5} height={17.5} />
    );
  } else {
    return active ? (
      <HeartDarkGreen width={19.5} height={17.5} />
    ) : (
      <HeartDark width={19.5} height={17.5} />
    );
  }
};
