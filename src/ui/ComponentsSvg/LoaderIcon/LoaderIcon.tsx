import React, {useContext} from 'react';
import {ThemeContext} from '../../../context/ThemeContext';
import LoaderSvg from '../../../assets/svg/loader.svg';
import LoaderLightSvg from '../../../assets/svg/loaderLight.svg';

type Props = {
  height?: number;
  width?: number;
};

export const LoaderIcon = (props: Props) => {
  const {height = 20, width = 20} = props;
  const {isDarkMode} = useContext(ThemeContext);

  if (isDarkMode) {
    return <LoaderSvg height={height} width={width} />;
  }

  return <LoaderLightSvg height={height} width={width} />;
};
