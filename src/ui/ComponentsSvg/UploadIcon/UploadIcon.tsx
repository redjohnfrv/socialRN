import React, {useContext} from 'react';
import {ThemeContext} from '../../../context/ThemeContext';
import UploadDark from '../../../assets/svg/uploadDark.svg';
import UploadLight from '../../../assets/svg/uploadLight.svg';

export const UploadIcon = () => {
  const {isDarkMode} = useContext(ThemeContext);

  if (isDarkMode) {
    return <UploadDark width={31.5} height={25} />;
  }

  return <UploadLight width={31.5} height={25} />;
};
