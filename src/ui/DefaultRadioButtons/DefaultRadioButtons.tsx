import React from 'react';
import {StyleSheet, View} from 'react-native';
import {RadioButton} from 'react-native-paper';
import {returnString} from '../../utils';
import {RadioButtonIcon} from '../ComponentsSvg';
import {DefaultText} from '../DefaultText';

type Props = {
  firstValue: string;
  secondValue: string;
  value: string;
  onChange: (...event: any[]) => void;
};

export const DefaultRadioButtons = (props: Props) => {
  const {firstValue, secondValue, value, onChange} = props;
  const [checked, setChecked] = React.useState<string>(value);

  return (
    <View style={styles.root}>
      <View>
        <View style={styles.radio}>
          <RadioButton
            key={firstValue}
            value={firstValue}
            status={checked === firstValue ? 'checked' : 'unchecked'}
            onPress={() => {
              setChecked(firstValue);
              onChange(firstValue);
            }}
          />
        </View>

        <View style={styles.pseudoRadio}>
          <View style={styles.indent}>
            <RadioButtonIcon
              key={firstValue + '1'}
              checked={checked === firstValue}
            />
          </View>
          <DefaultText
            key={firstValue + '2'}
            text={returnString.toCapitalize(firstValue)}
            font="Body5_Regular_16pt"
            numberOfLines={1}
          />
        </View>
      </View>

      <View>
        <View style={styles.radio}>
          <RadioButton
            key={secondValue}
            value={secondValue}
            status={checked === secondValue ? 'checked' : 'unchecked'}
            onPress={() => {
              setChecked(secondValue);
              onChange(secondValue);
            }}
          />
        </View>

        <View style={styles.pseudoRadio}>
          <View style={styles.indent}>
            <RadioButtonIcon
              key={secondValue + '1'}
              checked={checked === secondValue}
            />
          </View>
          <DefaultText
            key={secondValue + '2'}
            text={returnString.toCapitalize(secondValue)}
            font="Body5_Regular_16pt"
            numberOfLines={1}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    width: 200,
  },
  radio: {
    opacity: 0,
    zIndex: 1,
  },
  pseudoRadio: {
    position: 'absolute',
    top: 8,
    left: 8,
    flexDirection: 'row',
    zIndex: 0,
  },
  indent: {
    marginRight: 8,
  },
});
