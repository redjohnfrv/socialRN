import React, {useCallback, useMemo} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useThemeColors} from '../../hooks/useThemeColors';
import {PostFilterType} from '../../__generated__/types';
import {typography} from '../../assets/styles/typography';
import {BUTTON_HIGHLIGHTS} from '../../constants/common';

type Props = {
  isDisabled?: boolean;
  onPress: (filter: PostFilterType) => void;
  tab: PostFilterType;
};

export const Tabs = (props: Props) => {
  const {isDisabled = false, onPress, tab} = props;
  const {modeColor, modeColorInverse} = useThemeColors();

  const tabsStylesAdapter = useMemo(
    () => ({
      Active: {
        backgroundColor: modeColorInverse.primary.default,
        color: modeColor.grayscale[100],
      },
      unActive: {
        backgroundColor: modeColor.grayscale[150],
        color: modeColor.grayscale[700],
      },
    }),
    [modeColor.grayscale, modeColorInverse.primary.default],
  );

  const getTabStyles = (tabName: PostFilterType) => {
    return tab === tabName
      ? tabsStylesAdapter.Active
      : tabsStylesAdapter.unActive;
  };

  const onTabClick = useCallback(
    (activeTab: PostFilterType) => {
      onPress(activeTab);
    },
    [onPress],
  );

  return (
    <View style={styles.root}>
      <TouchableOpacity
        disabled={isDisabled}
        activeOpacity={BUTTON_HIGHLIGHTS.disable}
        onPress={() => onTabClick(PostFilterType.New)}
        style={[
          styles.leftTab,
          {backgroundColor: getTabStyles(PostFilterType.New).backgroundColor},
        ]}>
        <Text
          style={[styles.tab, {color: getTabStyles(PostFilterType.New).color}]}>
          {PostFilterType.New}
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        disabled={isDisabled}
        activeOpacity={BUTTON_HIGHLIGHTS.disable}
        onPress={() => onTabClick(PostFilterType.Top)}
        style={[
          styles.rightTab,
          {backgroundColor: getTabStyles(PostFilterType.Top).backgroundColor},
        ]}>
        <Text
          style={[styles.tab, {color: getTabStyles(PostFilterType.Top).color}]}>
          {PostFilterType.Top}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const tabStyles = {
  alignItems: 'center',
  flex: 1,
  paddingVertical: 12,
} as const;

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
  },
  leftTab: {
    ...tabStyles,
    borderTopLeftRadius: 16,
    borderBottomLeftRadius: 16,
  },
  rightTab: {
    ...tabStyles,
    borderTopRightRadius: 16,
    borderBottomRightRadius: 16,
  },
  tab: {
    ...typography.Body4_Regular_18pt,
  },
});
