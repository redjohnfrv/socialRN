import React from 'react';
import {TouchableHighlight, TouchableHighlightProps} from 'react-native';
import {useSwitcher} from '../../hooks/useSwitcher';
import {SharedIcon} from '../ComponentsSvg';

type Props = {
  onButtonPress: () => void;
} & TouchableHighlightProps;

export const ShareButton = (props: Props) => {
  const {onButtonPress} = props;
  const {
    isOn: isPress,
    on: setIsPressOn,
    off: setIsPressOff,
  } = useSwitcher(false);

  return (
    <TouchableHighlight
      underlayColor={'transparent'}
      onPress={onButtonPress}
      onHideUnderlay={() => setIsPressOff()}
      onShowUnderlay={() => setIsPressOn()}>
      <SharedIcon active={isPress} />
    </TouchableHighlight>
  );
};
