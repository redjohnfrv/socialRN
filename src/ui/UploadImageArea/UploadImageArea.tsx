import React, {useContext} from 'react';
import {
  Image,
  StyleSheet,
  TouchableOpacity,
  TouchableOpacityProps,
  View,
} from 'react-native';
import {useThemeColors} from '../../hooks/useThemeColors';
import {ThemeContext} from '../../context/ThemeContext';
import {colors} from '../../assets/styles/colors';
import {images} from '../../assets/images';
import {UploadIcon} from '../ComponentsSvg';
import {DefaultText} from '../DefaultText';

type Props = {
  uri: string | undefined;
  onPress: () => void;
} & TouchableOpacityProps;

export const UploadImageArea = (props: Props) => {
  const {uri, onPress, ...buttonProps} = props;
  const {isDarkMode} = useContext(ThemeContext);
  const {modeColorInverse} = useThemeColors();

  const uploaderWrapperStyles = {
    backgroundColor: isDarkMode
      ? colors.dark.grayscale[200]
      : colors.light.grayscale[150],
    borderColor: uri ? 'transparent' : modeColorInverse.primary.default,
  };

  return (
    <TouchableOpacity
      {...buttonProps}
      onPress={onPress}
      style={[styles.root, uploaderWrapperStyles]}>
      {!uri ? (
        <>
          <UploadIcon />

          <DefaultText
            text="Upload your photo here"
            font="Body3_Medium_14pt"
            style={styles.uploadText}
          />
        </>
      ) : (
        <View style={styles.imageWrapper}>
          <Image style={styles.image} source={uri ? {uri} : images.noImage} />
        </View>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 166,
    marginBottom: 20,
    borderRadius: 24,
    borderWidth: 1.5,
    borderStyle: 'dashed',
  },
  uploadText: {
    marginTop: 8,
  },
  imageWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    width: '100%',
    height: '100%',
    borderRadius: 24,
  },
});
