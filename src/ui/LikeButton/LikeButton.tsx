import React from 'react';
import {
  GestureResponderEvent,
  TouchableHighlight,
  TouchableHighlightProps,
} from 'react-native';
import {HeartIcon} from '../ComponentsSvg';

type Props = {
  active?: boolean;
  onLikeButtonPress: () => void;
} & TouchableHighlightProps;

export const LikeButton = (props: Props) => {
  const {active = false, disabled, onPress, onLikeButtonPress} = props;

  return (
    <TouchableHighlight
      underlayColor={'transparent'}
      disabled={disabled}
      onPress={(e: GestureResponderEvent) => {
        onPress && onPress(e);
        onLikeButtonPress();
      }}>
      <HeartIcon active={active} />
    </TouchableHighlight>
  );
};
