import React from 'react';
import {StyleSheet, View} from 'react-native';
import {AnimatedRotate} from '../Animated';
import {LoaderIcon} from '../ComponentsSvg';

export const LoaderEmptyContent = () => {
  return (
    <View style={styles.root}>
      <AnimatedRotate>
        <LoaderIcon width={40} height={40} />
      </AnimatedRotate>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 'auto',
  },
});
