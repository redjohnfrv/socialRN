import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableOpacityProps,
} from 'react-native';
import {colors} from '../../assets/styles/colors';
import {typography} from '../../assets/styles/typography';
import DeleteIcon from '../../assets/svg/trash.svg';

type Props = {} & TouchableOpacityProps;

export const DeleteSwipeButton = (props: Props) => {
  return (
    <TouchableOpacity style={styles.root} activeOpacity={0.8} {...props}>
      <DeleteIcon width={24} height={28} />
      <Text style={styles.text}>Delete</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  root: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '99%',
    paddingHorizontal: 16,
    backgroundColor: colors.additional.error,
  },
  text: {
    marginTop: 10,
    color: '#FFF',
    ...typography.Body3_Medium_14pt,
  },
});
