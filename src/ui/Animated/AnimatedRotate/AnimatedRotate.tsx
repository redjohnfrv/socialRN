import React, {useRef, ReactNode, useCallback} from 'react';
import {Animated, Easing} from 'react-native';

type Props = {
  style?: Record<string, any>;
  children: ReactNode;
};

export const AnimatedRotate = (props: Props) => {
  const {style, children} = props;
  const rotateAnimation = useRef(new Animated.Value(0)).current;

  const getRotationAnimation = () => {
    const rotate = rotateAnimation.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });
    return {rotate};
  };

  const animation = useCallback(() => {
    Animated.sequence([
      Animated.timing(rotateAnimation, {
        easing: Easing.linear,
        toValue: 1,
        duration: 3000,
        delay: 0,
        useNativeDriver: true,
      }),
      Animated.timing(rotateAnimation, {
        easing: Easing.linear,
        toValue: 0,
        duration: 0,
        delay: 0,
        useNativeDriver: true,
      }),
    ]).start(event => {
      if (event.finished) {
        animation();
      }
    });
  }, [rotateAnimation]);

  animation();

  return (
    <Animated.View
      style={{
        ...style,
        transform: [getRotationAnimation()],
      }}>
      {children}
    </Animated.View>
  );
};
