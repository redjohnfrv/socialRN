import React, {useContext} from 'react';
import {
  StyleProp,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableHighlightProps,
  View,
  ViewStyle,
} from 'react-native';
import {useButtonStyles} from '../../hooks/useButtonStyles';
import {CopyLinkButtonVariant} from '../../types';
import {ThemeContext} from '../../context/ThemeContext';
import {AnimatedRotate} from '../Animated';
import {LoaderIcon, CopyLinkIcon} from '../ComponentsSvg';

type Props = {
  customStyles?: StyleProp<ViewStyle>;
  isLoading?: boolean;
} & TouchableHighlightProps;

export const CopyLinkButton = (props: Props) => {
  const {isDarkMode} = useContext(ThemeContext);

  const {
    onPress,
    customStyles,
    disabled,
    isLoading = false,
    ...buttonProps
  } = props;

  const isDisabled = disabled || isLoading;

  const {
    isPress,
    touchProps,
    pressedButtonStyles,
    disabledButtonStyles,
    initialCopyLinkButtonStyles,
  } = useButtonStyles('textIcon', onPress, customStyles, isDisabled, isLoading);

  const pressedStyles = isPress
    ? pressedButtonStyles.color
    : initialCopyLinkButtonStyles.color;

  const colorStyles = isDisabled ? disabledButtonStyles.color : pressedStyles;

  const getVariant = (): CopyLinkButtonVariant => {
    if (isDarkMode) {
      if (isDisabled) {
        return 'gray';
      }
      if (isPress) {
        return 'black';
      }
      return 'white';
    } else {
      if (isDisabled) {
        return 'gray';
      }
      if (isPress) {
        return 'white';
      }
      return 'white';
    }
  };

  return (
    <TouchableHighlight {...touchProps} {...buttonProps} disabled={isDisabled}>
      {isLoading ? (
        <AnimatedRotate style={styles.rotateStyles}>
          <LoaderIcon />
        </AnimatedRotate>
      ) : (
        <View style={styles.content}>
          <Text
            style={{
              color: colorStyles,
            }}>
            Copy Link
          </Text>
          <CopyLinkIcon variant={getVariant()} />
        </View>
      )}
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  rotateStyles: {
    transform: [{rotate: '0deg'}],
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
