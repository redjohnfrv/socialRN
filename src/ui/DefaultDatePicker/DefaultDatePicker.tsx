import React, {useContext, useState} from 'react';
import DatePicker from 'react-native-date-picker';
import {ThemeContext} from '../../context/ThemeContext';
import {colors} from '../../assets/styles/colors';
import {DefaultInput} from '../DefaultInput';

type Props = {
  value: Date;
  onChange: (...event: any[]) => void;
};

export const DefaultDatePicker = (props: Props) => {
  const {value, onChange} = props;
  const [date, setDate] = useState(value);
  const [open, setOpen] = useState(false);
  const {isDarkMode} = useContext(ThemeContext);

  const onConfirm = (dateValue: Date) => {
    setOpen(false);
    setDate(dateValue);
    onChange && onChange(dateValue);
  };

  return (
    <>
      <DefaultInput
        title="B-Date"
        placeholder="Select date of birth"
        showSoftInputOnFocus={false}
        onFocus={evt => {
          evt.preventDefault();
          setOpen(true);
        }}
        value={date.toLocaleDateString()}
      />

      <DatePicker
        modal
        mode="date"
        locale="en"
        open={open}
        title="Pick the date of your birth"
        androidVariant={'iosClone'}
        fadeToColor="none"
        textColor={
          isDarkMode ? colors.light.grayscale[700] : colors.dark.grayscale[100]
        }
        date={date}
        onConfirm={itemDate => onConfirm(itemDate)}
        onCancel={() => {
          setOpen(false);
        }}
      />
    </>
  );
};
