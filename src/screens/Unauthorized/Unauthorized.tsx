import React from 'react';
import {AuthNavigator} from '../../navigators';

export const Unauthorized = () => {
  return <AuthNavigator />;
};
