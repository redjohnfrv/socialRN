import React from 'react';
import {ROUTES} from '../../../../constants/routes';
import {ScrollView, StyleSheet, View} from 'react-native';
import {Controller, useForm} from 'react-hook-form';
import {ApolloError} from '@apollo/client';
import {useUserSignIn} from '../../../../graphql/mutations/__generated__/userSignIn.mutation';
import {useToken} from '../../../../hooks/useToken';
import {useAppToast} from '../../../../hooks/useAppToast';
import {regexp} from '../../../../constants/regexp';
import {returnBoolean} from '../../../../utils';
import {DefaultInput, SecureInput} from '../../../../ui';
import {DefaultLayout} from '../../../../layouts';
import {BottomAuthBlock, TopAuthBlock} from '../../../../components';

type LoginFormType = {
  email: string;
  password: string;
};

export const LoginScreen = () => {
  const [sendAuthData, {error, loading}] = useUserSignIn();
  const showToast = useAppToast();
  const {setToken} = useToken();

  const {control, handleSubmit, formState, getValues} = useForm<LoginFormType>({
    defaultValues: {
      email: '',
      password: '',
    },
    reValidateMode: 'onChange',
    mode: 'onChange',
  });

  const onSubmit = async (formData: LoginFormType) => {
    const {email, password} = formData;

    if (email && password) {
      try {
        const {data} = await sendAuthData({
          variables: {
            input: {
              email,
              password,
            },
          },
        });

        if (data?.userSignIn.token) {
          setToken(data.userSignIn.token);
        } else if (data?.userSignIn.problem) {
          showToast(data.userSignIn.problem.message, 'danger');
        }
      } catch (err) {
        if (error instanceof ApolloError) {
          showToast(error.message, 'danger');
        } else {
          showToast('Unknown error. Please connect with developers', 'danger');
        }
      }
    }
  };

  const {isSubmitting, errors, dirtyFields} = formState;
  const isButtonDisabled =
    isSubmitting ||
    !dirtyFields.email ||
    returnBoolean.hasErrorsOnForm(
      errors.email?.message,
      errors.password?.message,
    ) ||
    !getValues('email') ||
    !getValues('password');

  return (
    <DefaultLayout>
      <View style={styles.topWrapper}>
        <TopAuthBlock
          title="Log in"
          text="You will be able to fully communicate"
        />
      </View>

      <View style={styles.formWrapper}>
        <ScrollView>
          <Controller
            control={control}
            rules={{
              required: {value: true, message: 'Enter correct e-mail'},
              pattern: {value: regexp.email, message: 'Enter correct e-mail'},
            }}
            render={({field: {onChange, onBlur, value}}) => (
              <DefaultInput
                onBlur={onBlur}
                value={value}
                customStyles={styles.input}
                placeholder="Enter your E-mail"
                title="E-mail"
                error={errors.email?.message}
                onChangeText={onChange}
              />
            )}
            name="email"
          />

          <Controller
            control={control}
            rules={{
              maxLength: {
                value: 20,
                message: 'The password field must not exceed 20 characters',
              },
              minLength: {
                value: 5,
                message: 'Password field must be at least 5 characters',
              },
              required: {
                value: true,
                message: 'Password field must be at least 5 characters',
              },
            }}
            render={({field: {onChange, onBlur, value}}) => (
              <SecureInput
                onBlur={onBlur}
                value={value}
                customStyles={styles.input}
                onChangeText={onChange}
                placeholder="Enter your password"
                title="Password"
                error={errors.password?.message}
              />
            )}
            name="password"
          />
        </ScrollView>
      </View>

      <BottomAuthBlock
        buttonTitle="Continue"
        linkTitle="Register"
        linkText="No account?"
        linkRoute={ROUTES.ROUTE_REGISTER}
        onPressButton={handleSubmit(onSubmit)}
        isButtonDisabled={isButtonDisabled}
        isLoading={loading}
      />
    </DefaultLayout>
  );
};

const styles = StyleSheet.create({
  topWrapper: {
    marginBottom: 40,
  },
  formWrapper: {
    marginBottom: 184,
  },
  input: {
    marginBottom: 12,
  },
});
