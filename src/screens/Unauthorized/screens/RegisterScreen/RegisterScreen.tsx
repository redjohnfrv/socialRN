import React from 'react';
import {useForm, Controller} from 'react-hook-form';
import {ScrollView, StyleSheet, View} from 'react-native';
import {ApolloError} from '@apollo/client';
import {useUserSignUp} from '../../../../graphql/mutations/__generated__/userSignUp.mutation';
import {useToken} from '../../../../hooks/useToken';
import {useAppToast} from '../../../../hooks/useAppToast';
import {returnBoolean} from '../../../../utils';
import {regexp} from '../../../../constants/regexp';
import {DefaultLayout} from '../../../../layouts';
import {BottomAuthBlock, TopAuthBlock} from '../../../../components';
import {DefaultInput, SecureInput} from '../../../../ui';

type RegisterFormType = {
  email: string;
  password: string;
  passwordConfirm: string;
};

export const RegisterScreen = () => {
  const [sendAuthData, {error, loading}] = useUserSignUp();
  const showToast = useAppToast();
  const {setToken} = useToken();

  const {control, handleSubmit, formState, getValues} =
    useForm<RegisterFormType>({
      defaultValues: {
        email: '',
        password: '',
        passwordConfirm: '',
      },
      reValidateMode: 'onChange',
      mode: 'onChange',
    });

  const onSubmit = async (formData: RegisterFormType) => {
    const {email, password, passwordConfirm} = formData;

    if (email && password && passwordConfirm) {
      try {
        const {data} = await sendAuthData({
          variables: {
            input: {
              email,
              password,
              passwordConfirm,
            },
          },
        });

        if (data?.userSignUp.token) {
          setToken(data.userSignUp.token);
        } else if (data?.userSignUp.problem) {
          showToast(data.userSignUp.problem.message, 'danger');
        }
      } catch (err) {
        if (error instanceof ApolloError) {
          showToast(error.message, 'danger');
        } else {
          showToast('Unknown error. Please connect with developers', 'danger');
        }
      }
    }
  };

  const {isSubmitting, errors, dirtyFields} = formState;
  const isButtonDisabled =
    isSubmitting ||
    !dirtyFields.email ||
    returnBoolean.hasErrorsOnForm(
      errors.email?.message,
      errors.password?.message,
      errors.passwordConfirm?.message,
    ) ||
    !getValues('email') ||
    !getValues('password') ||
    !getValues('passwordConfirm');

  return (
    <DefaultLayout>
      <View style={styles.topWrapper}>
        <TopAuthBlock
          title="Join us"
          text="You will be able to fully communicate"
        />
      </View>

      <View style={styles.formWrapper}>
        <ScrollView>
          <Controller
            control={control}
            rules={{
              required: {value: true, message: 'Enter correct e-mail'},
              pattern: {value: regexp.email, message: 'Enter correct e-mail'},
            }}
            render={({field: {onChange, onBlur, value}}) => (
              <DefaultInput
                onBlur={onBlur}
                value={value}
                customStyles={styles.input}
                placeholder="Enter your E-mail"
                title="E-mail"
                error={errors.email?.message}
                onChangeText={onChange}
              />
            )}
            name="email"
          />

          <Controller
            control={control}
            rules={{
              maxLength: {
                value: 20,
                message: 'The password field must not exceed 20 characters',
              },
              minLength: {
                value: 5,
                message: 'Password field must be at least 5 characters',
              },
              required: {
                value: true,
                message: 'Password field must be at least 5 characters',
              },
            }}
            render={({field: {onChange, onBlur, value}}) => (
              <SecureInput
                onBlur={onBlur}
                value={value}
                customStyles={styles.input}
                onChangeText={onChange}
                placeholder="Enter your password"
                title="Password"
                error={errors.password?.message}
              />
            )}
            name="password"
          />

          <Controller
            control={control}
            rules={{
              required: {
                value: true,
                message: 'Both passwords must match',
              },
              validate: () =>
                getValues('password') !== getValues('passwordConfirm')
                  ? 'Both passwords must match'
                  : undefined,
            }}
            render={({field: {onChange, onBlur, value}}) => (
              <SecureInput
                onBlur={onBlur}
                value={value}
                customStyles={styles.input}
                onChangeText={onChange}
                placeholder="Confirm your password"
                title="Confirm password"
                error={errors.passwordConfirm?.message}
              />
            )}
            name="passwordConfirm"
          />
        </ScrollView>
      </View>

      <BottomAuthBlock
        buttonTitle="Continue"
        onPressButton={handleSubmit(onSubmit)}
        isButtonDisabled={isButtonDisabled}
        isLoading={loading}
      />
    </DefaultLayout>
  );
};

const styles = StyleSheet.create({
  topWrapper: {
    marginBottom: 40,
  },
  formWrapper: {
    marginBottom: 110,
  },
  input: {
    marginBottom: 12,
  },
});
