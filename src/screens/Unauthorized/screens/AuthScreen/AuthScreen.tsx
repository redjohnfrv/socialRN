import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {useThemeColors} from '../../../../hooks/useThemeColors';
import {images} from '../../../../assets/images';
import {typography} from '../../../../assets/styles/typography';
import {ROUTES} from '../../../../constants/routes';
import {BottomAuthBlock} from '../../../../components';
import {DefaultLayout} from '../../../../layouts';
import {colors} from '../../../../assets/styles/colors';

export const AuthScreen = () => {
  const {modeColor} = useThemeColors();
  const navigation = useNavigation<NativeStackNavigationProp<any>>();

  return (
    <DefaultLayout paddings={0}>
      <View style={[styles.root]}>
        <View style={styles.blocks}>
          <Text
            style={[
              styles.blockWrapper,
              {color: modeColor.grayscale[100]},
              {transform: [{rotate: '-3deg'}, {translateX: 25}]},
            ]}>
            SHARE
          </Text>
          <Text
            style={[
              styles.blockWrapper,
              {color: modeColor.grayscale[100]},
              {transform: [{rotate: '-9deg'}]},
            ]}>
            YOUR TALE
          </Text>
        </View>

        <BottomAuthBlock
          buttonTitle="Create an account"
          onPressButton={() => navigation.navigate(ROUTES.ROUTE_REGISTER)}
        />
      </View>

      <ImageBackground source={images.bg} style={styles.background} />

      <TouchableOpacity
        style={styles.uiButton}
        onPress={() => navigation.navigate(ROUTES.UI_ROUTE)}>
        <Text style={styles.uiText}>UI</Text>
      </TouchableOpacity>
    </DefaultLayout>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    paddingHorizontal: 16,
  },
  blocks: {
    position: 'absolute',
    top: 95,
    alignSelf: 'center',
  },
  blockWrapper: {
    paddingTop: 13,
    paddingBottom: 13,
    paddingLeft: 21,
    paddingRight: 21,
    alignSelf: 'flex-start',
    backgroundColor: colors.light.primary.default,
    ...typography.Title_1_Regular_55pt,
  },
  background: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    top: 0,
    left: 0,
    zIndex: -1,
  },
  uiButton: {
    position: 'absolute',
    alignItems: 'center',
    top: 32,
    right: 16,
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#000',
  },
  uiText: {
    top: 4,
    color: '#000',
    ...typography.Title_2_Medium_32pt,
  },
});
