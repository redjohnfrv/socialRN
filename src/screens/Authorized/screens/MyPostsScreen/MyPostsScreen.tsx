import React, {useCallback, useContext} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {useFocusEffect, useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import axios from 'axios';
import {
  MyPosts,
  useMyPosts,
} from '../../../../graphql/queries/__generated__/myPosts.query';
import {useUserMe} from '../../../../graphql/queries/__generated__/userMe.query';
import {usePostDelete} from '../../../../graphql/mutations/__generated__/postDelete.mutation';
import {useAppToast} from '../../../../hooks/useAppToast';
import {useToken} from '../../../../hooks/useToken';
import {RouteContext} from '../../../../context/RouteContext';
import {ROUTES} from '../../../../constants/routes';
import {PostCustomType} from '../../../../types/posts';
import {restAPI, s3_delete_file} from '../../../../constants/api';
import {UploadImageCategory} from '../../../../types';
import {returnString} from '../../../../utils';
import {NoContent, PostPreview, TopPageBlock} from '../../../../components';
import {DefaultLayout} from '../../../../layouts';
import {AddPostButton, LoaderEmptyContent} from '../../../../ui';

export const MyPostsScreen = () => {
  const {token} = useToken();
  const showToast = useAppToast();
  const {onRouteChange} = useContext(RouteContext);
  const {navigate} = useNavigation<NativeStackNavigationProp<any>>();
  const {data: userData} = useUserMe();
  const [deletePost, {loading: deletePostLoading, error: deletePostError}] =
    usePostDelete();

  const {
    data: postsData,
    error: postsError,
    loading: postsLoading,
    networkStatus,
    refetch,
    fetchMore,
  } = useMyPosts({
    variables: {
      input: {
        limit: 10,
      },
    },
  });

  const user = userData?.userMe;
  const myPosts = postsData?.myPosts;
  const noPosts = (!myPosts?.data?.length || postsError) && !postsLoading;

  const onDeleteMediaUrl = useCallback(
    async (image: string): Promise<boolean> => {
      if (image) {
        const configurationObject = {
          method: 'DELETE',
          url: `${restAPI}${s3_delete_file}?fileCategory=${UploadImageCategory.POSTS}&fileKey=${image}`,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        };

        try {
          await axios(configurationObject);
          return true;
        } catch {
          showToast('Cannot delete image...', 'danger');
        }

        return false;
      } else {
        return true;
      }
    },
    [showToast, token],
  );

  const onPostDelete = useCallback(
    async (postId: string, postMediaUrl: string) => {
      const isImageDeleted = await onDeleteMediaUrl(postMediaUrl);

      if (isImageDeleted) {
        const response = await deletePost({
          variables: {
            input: {
              id: postId,
            },
          },
        });

        if (response?.data?.postDelete?.ok) {
          showToast('Post successful deleted!', 'success');
          refetch();
        }

        if (deletePostError) {
          if (deletePostError?.message) {
            showToast(deletePostError.message, 'danger');
          } else {
            showToast(
              'Unknown error. Please connect with developers',
              'danger',
            );
          }
        }
      }
    },
    [deletePost, deletePostError, onDeleteMediaUrl, refetch, showToast],
  );

  const renderPostItem = ({item}: {item: PostCustomType}) => {
    const author = returnString.getPrettyAuthorName(
      item.author.firstName,
      item.author.lastName,
    );

    return (
      <PostPreview
        key={item.id}
        swipeAble={true}
        isLoading={deletePostLoading}
        onPress={() => onPostDelete(item.id, item.mediaUrl)}
        id={item.id}
        title={item.title ?? ''}
        date={item.createdAt}
        src={item.mediaUrl}
        isLiked={item.isLiked}
        likesCount={item.likesCount ?? 0}
        avatar={item.author.avatarUrl ?? ''}
        author={author}
      />
    );
  };

  const updateQuery = (
    previousResult: MyPosts,
    {fetchMoreResult}: {fetchMoreResult: MyPosts},
  ) => {
    if (!fetchMoreResult) {
      return previousResult;
    }

    if (myPosts?.pageInfo?.afterCursor) {
      const previousEdges = previousResult.myPosts.data;
      const fetchMoreEdges = fetchMoreResult.myPosts.data;

      fetchMoreResult.myPosts.data = [...previousEdges!, ...fetchMoreEdges!];
      return {...fetchMoreResult};
    }

    return previousResult;
  };

  useFocusEffect(
    useCallback(() => {
      onRouteChange(ROUTES.TAB_ROUTE_POSTS);
    }, [onRouteChange]),
  );

  useFocusEffect(
    useCallback(() => {
      refetch();
    }, [refetch]),
  );

  return (
    <DefaultLayout>
      <View style={styles.root}>
        <TopPageBlock title="My posts" avatarUrl={user?.avatarUrl ?? ''} />

        {postsLoading ? (
          <LoaderEmptyContent />
        ) : noPosts ? (
          <NoContent text="You haven't added anything to your posts yet" />
        ) : (
          <FlatList
            style={styles.list}
            data={myPosts?.data}
            renderItem={renderPostItem}
            keyExtractor={item => String(item.id)}
            onEndReachedThreshold={0.5}
            onEndReached={() => {
              fetchMore({
                variables: {
                  input: {
                    limit: 10,
                    afterCursor: myPosts?.pageInfo?.afterCursor,
                  },
                },
                updateQuery,
              });
            }}
            refreshing={networkStatus === 4}
            onRefresh={() => refetch()}
          />
        )}

        <View style={styles.addButton}>
          <AddPostButton onPress={() => navigate(ROUTES.ROUTE_CREATE_POST)} />
        </View>
      </View>
    </DefaultLayout>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  list: {
    marginHorizontal: -16,
  },
  addButton: {
    alignItems: 'flex-end',
    width: '100%',
    position: 'absolute',
    bottom: 32,
    right: 4,
  },
});
