import React, {useCallback, useContext} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import {useUserMe} from '../../../../graphql/queries/__generated__/userMe.query';
import {
  FavouritePosts,
  useFavouritePosts,
} from '../../../../graphql/queries/__generated__/favouritePosts.query';
import {RouteContext} from '../../../../context/RouteContext';
import {returnString} from '../../../../utils';
import {ROUTES} from '../../../../constants/routes';
import {PostCustomType} from '../../../../types/posts';
import {DefaultLayout} from '../../../../layouts';
import {LoaderEmptyContent} from '../../../../ui';
import {NoContent, PostPreview, TopPageBlock} from '../../../../components';

export const FavoritesScreen = () => {
  const {onRouteChange} = useContext(RouteContext);
  const {data: userData} = useUserMe();

  const {
    data: postsData,
    error: postsError,
    loading: postsLoading,
    networkStatus,
    refetch,
    fetchMore,
  } = useFavouritePosts({
    variables: {
      input: {
        limit: 10,
      },
    },
  });

  const user = userData?.userMe;
  const favouritesPosts = postsData?.favouritePosts;
  const noPosts =
    (!favouritesPosts?.data?.length || postsError) && !postsLoading;

  useFocusEffect(
    useCallback(() => {
      onRouteChange(ROUTES.TAB_ROUTE_FAVORITES);
    }, [onRouteChange]),
  );

  const renderPostItem = ({item}: {item: PostCustomType}) => {
    const author = returnString.getPrettyAuthorName(
      item.author.firstName,
      item.author.lastName,
    );

    return (
      <PostPreview
        key={item.id}
        id={item.id}
        title={item.title ?? ''}
        date={item.createdAt}
        src={item.mediaUrl}
        isLiked={item.isLiked}
        likesCount={item.likesCount ?? 0}
        avatar={item.author.avatarUrl ?? ''}
        author={author}
      />
    );
  };

  const updateQuery = (
    previousResult: FavouritePosts,
    {fetchMoreResult}: {fetchMoreResult: FavouritePosts},
  ) => {
    if (!fetchMoreResult) {
      return previousResult;
    }

    if (favouritesPosts?.pageInfo?.afterCursor) {
      const previousEdges = previousResult.favouritePosts.data;
      const fetchMoreEdges = fetchMoreResult.favouritePosts.data;

      fetchMoreResult.favouritePosts.data = [
        ...previousEdges!,
        ...fetchMoreEdges!,
      ];
      return {...fetchMoreResult};
    }

    return previousResult;
  };

  useFocusEffect(
    useCallback(() => {
      onRouteChange(ROUTES.TAB_ROUTE_FAVORITES);
    }, [onRouteChange]),
  );

  useFocusEffect(
    useCallback(() => {
      refetch();
    }, [refetch]),
  );

  return (
    <DefaultLayout flex={'start'}>
      <View style={styles.root}>
        <View style={styles.topBlock}>
          <TopPageBlock title="Favourites" avatarUrl={user?.avatarUrl ?? ''} />
        </View>

        {postsLoading ? (
          <LoaderEmptyContent />
        ) : noPosts ? (
          <NoContent text="You haven't added anything to your favorites yet" />
        ) : (
          <FlatList
            style={styles.list}
            data={favouritesPosts?.data}
            renderItem={renderPostItem}
            keyExtractor={item => String(item.id)}
            onEndReachedThreshold={0.5}
            onEndReached={() => {
              fetchMore({
                variables: {
                  input: {
                    limit: 10,
                    afterCursor: favouritesPosts?.pageInfo?.afterCursor,
                  },
                },
                updateQuery,
              });
            }}
            refreshing={networkStatus === 4}
            onRefresh={() => refetch()}
          />
        )}
      </View>
    </DefaultLayout>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  topBlock: {
    marginBottom: 24,
  },
  list: {
    marginHorizontal: -16,
  },
});
