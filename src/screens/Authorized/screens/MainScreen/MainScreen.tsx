import React, {useCallback, useContext, useState} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import {useToken} from '../../../../hooks/useToken';
import {
  Posts,
  usePosts,
} from '../../../../graphql/queries/__generated__/posts.query';
import {useUserMe} from '../../../../graphql/queries/__generated__/userMe.query';
import {PostFilterType} from '../../../../__generated__/types';
import {RouteContext} from '../../../../context/RouteContext';
import {returnString} from '../../../../utils';
import {ROUTES} from '../../../../constants/routes';
import {PostCustomType} from '../../../../types/posts';
import {DefaultLayout} from '../../../../layouts';
import {NoContent, PostPreview, TopPageBlock} from '../../../../components';
import {LoaderEmptyContent, Tabs} from '../../../../ui';

export const MainScreen = () => {
  const {token} = useToken();
  const {onRouteChange} = useContext(RouteContext);
  const [postsFilter, setPostsFilter] = useState(PostFilterType.New);
  const {data: userData} = useUserMe({
    context: {
      headers: {
        authorization: `Bearer ${token}`,
      },
    },
  });

  const {
    data: postsData,
    error: postsError,
    loading: postsLoading,
    networkStatus,
    refetch,
    fetchMore,
  } = usePosts({
    variables: {
      input: {
        limit: 10,
        type: postsFilter,
      },
    },
    context: {
      headers: {
        authorization: `Bearer ${token}`,
      },
    },
  });

  const user = userData?.userMe;
  const posts = postsData?.posts;
  const noPosts = (!posts?.data?.length || postsError) && !postsLoading;

  const renderPostItem = ({item}: {item: PostCustomType}) => {
    const author = returnString.getPrettyAuthorName(
      item.author.firstName,
      item.author.lastName,
    );

    return (
      <PostPreview
        key={item.id}
        id={item.id}
        title={item.title ?? ''}
        date={item.createdAt ?? ''}
        src={item.mediaUrl}
        isLiked={item.isLiked}
        likesCount={item.likesCount ?? 0}
        avatar={item.author.avatarUrl ?? ''}
        author={author}
      />
    );
  };

  const updateQuery = (
    previousResult: Posts,
    {fetchMoreResult}: {fetchMoreResult: Posts},
  ) => {
    if (!fetchMoreResult) {
      return previousResult;
    }

    if (posts?.pageInfo?.afterCursor) {
      const previousEdges = previousResult.posts.data;
      const fetchMoreEdges = fetchMoreResult.posts.data;

      fetchMoreResult.posts.data = [...previousEdges!, ...fetchMoreEdges!];
      return {...fetchMoreResult};
    }

    return previousResult;
  };

  useFocusEffect(
    useCallback(() => {
      onRouteChange(ROUTES.TAB_ROUTE_MAIN);
    }, [onRouteChange]),
  );

  useFocusEffect(
    useCallback(() => {
      refetch();
    }, [refetch]),
  );

  return (
    <DefaultLayout flex={'start'}>
      <View style={styles.root}>
        <TopPageBlock
          title={`Hello ${user?.firstName ?? 'dude'}!`}
          avatarUrl={user?.avatarUrl ?? ''}
        />

        <View style={styles.tabs}>
          <Tabs
            isDisabled={postsLoading}
            onPress={setPostsFilter}
            tab={postsFilter}
          />
        </View>

        {postsLoading ? (
          <LoaderEmptyContent />
        ) : noPosts ? (
          <NoContent text="Sorry... No any posts :(" />
        ) : (
          <FlatList
            style={styles.list}
            data={posts?.data}
            renderItem={renderPostItem}
            keyExtractor={item => String(item.id)}
            onEndReachedThreshold={0.5}
            onEndReached={() => {
              fetchMore({
                variables: {
                  input: {
                    type: postsFilter,
                    limit: 10,
                    afterCursor: posts?.pageInfo?.afterCursor,
                  },
                },
                updateQuery,
              });
            }}
            refreshing={networkStatus === 4}
            onRefresh={() => refetch()}
          />
        )}
      </View>
    </DefaultLayout>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  tabs: {
    marginTop: 20,
    marginBottom: 40,
  },
  list: {
    marginHorizontal: -16,
  },
});
