import React, {useCallback, useMemo, useState} from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import {Controller, useForm} from 'react-hook-form';
import {ApolloError} from '@apollo/client';
import RNFetchBlob from 'rn-fetch-blob';
import {usePostCreate} from '../../../../graphql/mutations/__generated__/postCreate.mutation';
import {useGetPictureWithPermission} from '../../../../hooks/useGetPictureWithPermission';
import {useSwitcher} from '../../../../hooks/useSwitcher';
import {useToken} from '../../../../hooks/useToken';
import {useAppToast} from '../../../../hooks/useAppToast';
import {
  getPresignedUrl,
  ImagePickerData,
} from '../../../../utils/uploadService';
import {CreatePostType} from '../../../../types/posts';
import {UploadImageCategory} from '../../../../types';
import {returnBoolean} from '../../../../utils';
import {DefaultLayout} from '../../../../layouts';
import {
  DefaultButton,
  DefaultInput,
  ImagePickerModal,
  UploadImageArea,
} from '../../../../ui';

export const CreatePostScreen = () => {
  const {token} = useToken();
  const showToast = useAppToast();
  const [uri, setUri] = useState<string | undefined>(undefined);
  const [imageData, setImageData] = useState<ImagePickerData>(undefined);
  const {getImage} = useGetPictureWithPermission();
  const [createPost, {loading: createPostLoading}] = usePostCreate();
  const {
    isOn: isVisible,
    off: setVisibleOff,
    on: setVisibleOn,
  } = useSwitcher(false);
  const {control, handleSubmit, formState, getValues, reset} =
    useForm<CreatePostType>({
      defaultValues: {
        description: '',
        title: '',
      },
      reValidateMode: 'onChange',
      mode: 'onChange',
    });
  const {isSubmitting, errors} = formState;
  const isButtonDisabled = useMemo(() => {
    return (
      isSubmitting ||
      returnBoolean.hasErrorsOnForm(
        errors.title?.message,
        errors.description?.message,
      ) ||
      !getValues('title') ||
      createPostLoading ||
      !uri
    );
  }, [
    createPostLoading,
    errors.description?.message,
    errors.title?.message,
    getValues,
    isSubmitting,
    uri,
  ]);

  const resetFormFields = useCallback(async () => {
    setUri(undefined);
    reset();
  }, [reset]);

  const onClearImageState = () => {
    setUri(undefined);
    setImageData(undefined);
    setVisibleOff();
  };

  const onSetImageState = useCallback(
    async (isCamera?: boolean) => {
      const image = await getImage(isCamera);
      setImageData(image);

      if (image?.data) {
        setUri(image?.source.uri || '');
        setVisibleOff();
      }
    },
    [getImage, setVisibleOff],
  );

  const onImageLibraryPress = useCallback(() => {
    onSetImageState(false);
  }, [onSetImageState]);

  const onCameraPress = useCallback(() => {
    onSetImageState(true);
  }, [onSetImageState]);

  /** AWS GET PRE-SIGNED, UPLOAD AND DELETE FNs **/
  const onAddImage = useCallback(async () => {
    const url = await getPresignedUrl(
      imageData?.filename!,
      token,
      UploadImageCategory.POSTS,
    );
    const response = await RNFetchBlob.fetch(
      'PUT',
      url,
      {'Content-Type': 'application/octet-stream'},
      imageData?.data.data,
    );

    if (response) {
      showToast('Image upload success!', 'success');
    }

    return url;
  }, [imageData?.data.data, imageData?.filename, showToast, token]);
  /** END OF AWS FNs **/

  const onSubmit = async (formData: CreatePostType) => {
    const {title, description} = formData;
    let tmpUrl: string = '';

    try {
      tmpUrl = await onAddImage();

      const {data} = await createPost({
        variables: {
          input: {
            mediaUrl: tmpUrl,
            title,
            description,
          },
        },
      });

      if (data?.postCreate.title) {
        showToast(`Post "${data?.postCreate.title}" added!`, 'success');
        resetFormFields();
      }
    } catch (err) {
      if (err instanceof ApolloError) {
        showToast(err.message, 'danger');
      } else {
        showToast('Unknown error. Please connect with developers', 'danger');
      }
    }
  };

  return (
    <DefaultLayout
      flex="start"
      onClickBackEffect={resetFormFields}
      topPanelTitle="Create post">
      <ScrollView>
        <UploadImageArea uri={uri} onPress={() => setVisibleOn()} />

        <Controller
          control={control}
          rules={{
            required: {value: true, message: 'Enter post title'},
            minLength: {
              value: 3,
              message: 'Minimum length 3',
            },
          }}
          render={({field: {onChange, onBlur, value}}) => (
            <DefaultInput
              onBlur={onBlur}
              value={value}
              customStyles={styles.title}
              placeholder="Enter title of post"
              title="Title"
              error={errors.title?.message}
              onChangeText={onChange}
            />
          )}
          name="title"
        />

        <Controller
          control={control}
          rules={{
            required: {value: true, message: 'Enter post message'},
            minLength: {
              value: 40,
              message: 'Minimum length 40',
            },
          }}
          render={({field: {onChange, onBlur, value}}) => (
            <DefaultInput
              customStyles={styles.description}
              onBlur={onBlur}
              value={value}
              placeholder="Enter your post"
              title="Post"
              multiline={true}
              error={errors.description?.message}
              onChangeText={onChange}
            />
          )}
          name="description"
        />

        <DefaultButton
          title="Publish"
          type="large"
          disabled={isButtonDisabled}
          onPress={handleSubmit(onSubmit)}
        />
      </ScrollView>

      <ImagePickerModal
        isVisible={isVisible}
        onClose={() => setVisibleOff()}
        onImageLibraryPress={onImageLibraryPress}
        onCameraPress={onCameraPress}
        onDeleteImage={onClearImageState}
      />
    </DefaultLayout>
  );
};

const styles = StyleSheet.create({
  title: {
    marginBottom: 20,
  },
  description: {
    marginBottom: 52,
  },
});
