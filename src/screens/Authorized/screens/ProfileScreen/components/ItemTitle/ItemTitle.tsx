import React from 'react';
import {StyleSheet} from 'react-native';
import {DefaultText} from '../../../../../../ui';

type Props = {
  title: string;
};

export const ItemTitle = (props: Props) => {
  const {title} = props;

  return (
    <DefaultText text={title} font="Body1_Medium_18pt" style={styles.indent} />
  );
};

const styles = StyleSheet.create({
  indent: {
    marginBottom: 16,
  },
});
