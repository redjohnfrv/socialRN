import React, {useCallback, useMemo, useState} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {Controller, useForm} from 'react-hook-form';
import {ApolloError} from '@apollo/client';
import axios from 'axios';
import RNFetchBlob from 'rn-fetch-blob';
import {useUserMe} from '../../../../graphql/queries/__generated__/userMe.query';
import {useUserEditProfile} from '../../../../graphql/mutations/__generated__/userEditProfile.mutation';
import {useSwitcher} from '../../../../hooks/useSwitcher';
import {useToken} from '../../../../hooks/useToken';
import {useAppToast} from '../../../../hooks/useAppToast';
import {useGetPictureWithPermission} from '../../../../hooks/useGetPictureWithPermission';
import {UploadImageCategory} from '../../../../types';
import {GenderType} from '../../../../__generated__/types';
import {regexp} from '../../../../constants/regexp';
import {
  getPresignedUrl,
  ImagePickerData,
} from '../../../../utils/uploadService';
import {returnBoolean, returnString} from '../../../../utils';
import {INPUTS_MAX_VALUES} from '../../../../constants/common';
import {restAPI, s3_delete_file} from '../../../../constants/api';
import {ProfileFormType} from '../../../../types/profile';
import {DefaultLayout} from '../../../../layouts';
import {ItemTitle} from './components';
import {ProfileAvatar} from '../../../../components';
import {
  DefaultDatePicker,
  DefaultInput,
  DefaultRadioButtons,
  DefaultTextButton,
  ImagePickerModal,
} from '../../../../ui';

export const ProfileScreen = () => {
  const {token} = useToken();
  const {getPrettyNumber, getValidDate, getS3AvatarUrl} = returnString;
  const [sendUserData, {loading: editLoading}] = useUserEditProfile();
  const showToast = useAppToast();
  const [imageData, setImageData] = useState<ImagePickerData>(undefined);
  const {data: userData, loading: userLoading} = useUserMe();
  const user = userData?.userMe;

  const [uri, setUri] = useState(user?.avatarUrl || undefined);
  const {getImage} = useGetPictureWithPermission();

  const {
    isOn: isVisible,
    off: setVisibleOff,
    on: setVisibleOn,
  } = useSwitcher(false);

  const {control, handleSubmit, formState, getValues, reset} =
    useForm<ProfileFormType>({
      defaultValues: {
        birthDate: user?.birthDate ? new Date(user?.birthDate) : new Date(),
        country: user?.country ?? '',
        email: user?.email ?? '',
        firstName: user?.firstName ?? '',
        gender: user?.gender ?? 'Male',
        id: user?.id ?? '',
        lastName: user?.lastName ?? '',
        middleName: user?.middleName ?? '',
        phone: user?.phone ?? '',
      },
      reValidateMode: 'onChange',
      mode: 'onChange',
    });

  const {isSubmitting, errors} = formState;
  const isButtonDisabled = useMemo(() => {
    return (
      isSubmitting ||
      returnBoolean.hasErrorsOnForm(
        errors.email?.message,
        errors.phone?.message,
        errors.firstName?.message,
        errors.lastName?.message,
      ) ||
      !getValues('email') ||
      userLoading ||
      editLoading
    );
  }, [
    editLoading,
    errors.email?.message,
    errors.firstName?.message,
    errors.lastName?.message,
    errors.phone?.message,
    getValues,
    isSubmitting,
    userLoading,
  ]);

  const resetFormFields = useCallback(async () => {
    setUri(user?.avatarUrl ?? '');
    reset();
  }, [reset, user?.avatarUrl]);

  const onClearAvatarState = () => {
    setUri(undefined);
    setImageData(undefined);
    setVisibleOff();
  };

  const onSetAvatarState = useCallback(
    async (isCamera?: boolean) => {
      const image = await getImage(isCamera);
      setImageData(image);

      if (image?.data) {
        setUri(image?.source.uri || '');
        setVisibleOff();
      }
    },
    [getImage, setVisibleOff],
  );

  /** AWS GET PRE-SIGNED, UPLOAD AND DELETE FNs **/
  const onDeleteAvatarUrl = useCallback(async () => {
    if (user?.avatarUrl) {
      const configurationObject = {
        method: 'DELETE',
        url: `${restAPI}${s3_delete_file}?fileCategory=${
          UploadImageCategory.AVATARS
        }&fileKey=${getS3AvatarUrl(user.avatarUrl)}`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };

      try {
        await axios(configurationObject);
        setUri(undefined);
      } catch {
        showToast('Cannot delete image...', 'danger');
      }
    }

    return null;
  }, [getS3AvatarUrl, showToast, token, user?.avatarUrl]);

  const onAddAvatar = useCallback(async () => {
    if (imageData) {
      const url = await getPresignedUrl(
        imageData?.filename!,
        token,
        UploadImageCategory.AVATARS,
      );
      const response = await RNFetchBlob.fetch(
        'PUT',
        url,
        {'Content-Type': 'application/octet-stream'},
        imageData?.data.data,
      );

      if (response) {
        showToast('Avatar upload success!', 'success');
      }

      return url;
    }

    return null;
  }, [imageData, showToast, token]);
  /** END OF AWS FNs **/

  const onSubmit = async (formData: ProfileFormType) => {
    const {birthDate, country, email, gender, middleName, lastName, firstName} =
      formData;
    let tmpUrl: string | null;

    if (uri) {
      tmpUrl = await onAddAvatar();
    } else {
      tmpUrl = await onDeleteAvatarUrl();
    }

    try {
      const {data} = await sendUserData({
        variables: {
          input: {
            avatarUrl: tmpUrl,
            birthDate: birthDate ? getValidDate(birthDate) : '',
            country,
            email,
            gender: gender === 'MALE' ? GenderType.Male : GenderType.Female,
            middleName,
            lastName,
            firstName,
          },
        },
      });

      if (data?.userEditProfile.problem) {
        if (
          data?.userEditProfile.problem.__typename === 'EmailAlreadyUsedProblem'
        ) {
          showToast('E-mail already in use', 'danger');
        }
        if (
          data?.userEditProfile.problem.__typename === 'PhoneAlreadyUsedProblem'
        ) {
          showToast('Phone already in use', 'danger');
        }
      }

      if (data?.userEditProfile.user) {
        showToast('Profile update successful', 'success');
      }
    } catch (err) {
      if (err instanceof ApolloError) {
        showToast(
          err?.message === 'VALIDATION_ERROR'
            ? 'Wrong user input data'
            : err.message,
          'danger',
        );
      } else {
        showToast('Unknown error. Please connect with developers', 'danger');
      }
    }
  };

  const onImageLibraryPress = useCallback(() => {
    onSetAvatarState(false);
  }, [onSetAvatarState]);

  const onCameraPress = useCallback(() => {
    onSetAvatarState(true);
  }, [onSetAvatarState]);

  return (
    <DefaultLayout
      topPanelTitle="Profile"
      flex="start"
      onClickBackEffect={resetFormFields}
      RightButton={() => (
        <DefaultTextButton
          title="Done"
          onPress={handleSubmit(onSubmit)}
          disabled={isButtonDisabled}
        />
      )}>
      <ScrollView>
        <View style={styles.doubleIndent}>
          <ProfileAvatar uri={uri} onPress={() => setVisibleOn()} />
        </View>

        {/*PERSONAL INFO*/}
        <ItemTitle title="Personal info" />

        <Controller
          control={control}
          rules={{
            required: {value: true, message: 'Enter your name'},
          }}
          render={({field: {onChange, onBlur, value}}) => (
            <DefaultInput
              title="First name"
              placeholder="Enter your first name"
              onBlur={onBlur}
              value={value}
              error={errors.firstName?.message}
              onChangeText={onChange}
              customStyles={styles.indent}
              maxLength={INPUTS_MAX_VALUES.firstName}
            />
          )}
          name="firstName"
        />

        <Controller
          control={control}
          rules={{
            required: {value: true, message: 'Enter your last name'},
          }}
          render={({field: {onChange, onBlur, value}}) => (
            <DefaultInput
              title="Last name"
              placeholder="Enter your last name"
              onBlur={onBlur}
              value={value}
              error={errors.lastName?.message}
              onChangeText={onChange}
              customStyles={styles.indent}
              maxLength={INPUTS_MAX_VALUES.lastName}
            />
          )}
          name="lastName"
        />

        <Controller
          control={control}
          render={({field: {onChange, onBlur, value}}) => (
            <DefaultInput
              title="Surname"
              placeholder="Enter your surname"
              onBlur={onBlur}
              value={value}
              error={errors.middleName?.message}
              onChangeText={onChange}
              customStyles={styles.indent}
              maxLength={INPUTS_MAX_VALUES.middleName}
            />
          )}
          name="middleName"
        />

        <ItemTitle title="Gender" />

        <Controller
          control={control}
          render={({field: {onChange, value}}) => (
            <View style={styles.doubleIndent}>
              <DefaultRadioButtons
                firstValue="MALE"
                secondValue="FEMALE"
                value={value}
                onChange={onChange}
              />
            </View>
          )}
          name="gender"
        />

        <ItemTitle title="Date of Birth" />

        <Controller
          control={control}
          render={({field: {onChange, value}}) => (
            <View style={styles.doubleIndent}>
              <DefaultDatePicker value={value} onChange={onChange} />
            </View>
          )}
          name="birthDate"
        />

        {/*ACCOUNT INFO*/}
        <ItemTitle title="Account info" />

        <Controller
          control={control}
          rules={{
            required: {value: true, message: 'Enter correct e-mail'},
            pattern: {value: regexp.email, message: 'Enter correct e-mail'},
          }}
          render={({field: {onChange, onBlur, value}}) => (
            <DefaultInput
              title="E-mail"
              placeholder="Enter your e-mail"
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
              error={errors.email?.message}
              customStyles={styles.doubleIndent}
              maxLength={INPUTS_MAX_VALUES.email}
            />
          )}
          name="email"
        />

        <Controller
          control={control}
          rules={{
            pattern: {value: regexp.phone, message: 'Enter correct phone'},
          }}
          render={({field: {onChange, onBlur, value}}) => {
            return (
              <DefaultInput
                title="Phone number"
                keyboardType="numeric"
                placeholder="Enter your phone"
                onBlur={onBlur}
                onChangeText={onChange}
                value={getPrettyNumber(value)}
                error={errors.phone?.message}
                maxLength={INPUTS_MAX_VALUES.phone}
                customStyles={styles.doubleIndent}
              />
            );
          }}
          name="phone"
        />

        <Controller
          control={control}
          render={({field: {onChange, onBlur, value}}) => (
            <DefaultInput
              title="Country"
              placeholder="Enter your country"
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
              error={errors.country?.message}
              customStyles={styles.doubleIndent}
              maxLength={INPUTS_MAX_VALUES.country}
            />
          )}
          name="country"
        />
      </ScrollView>

      <ImagePickerModal
        isVisible={isVisible}
        onClose={() => setVisibleOff()}
        onImageLibraryPress={onImageLibraryPress}
        onCameraPress={onCameraPress}
        onDeleteImage={onClearAvatarState}
      />
    </DefaultLayout>
  );
};

const styles = StyleSheet.create({
  indent: {
    marginBottom: 16,
  },
  doubleIndent: {
    marginBottom: 32,
  },
});
