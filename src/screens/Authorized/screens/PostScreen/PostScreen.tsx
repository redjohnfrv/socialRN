import React, {useCallback, useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {usePost} from '../../../../graphql/queries/__generated__/post.query';
import {useFocusEffect} from '@react-navigation/native';
import {useAppToast} from '../../../../hooks/useAppToast';
import {colors} from '../../../../assets/styles/colors';
import {typography} from '../../../../assets/styles/typography';
import {returnString} from '../../../../utils';
import {images} from '../../../../assets/images';
import {DefaultText, LoaderEmptyContent} from '../../../../ui';
import {DefaultLayout} from '../../../../layouts';
import {PostBottom} from '../../../../components/PostPreview/components';

type Props = {} & NativeStackScreenProps<any>;

export const PostScreen = (props: Props) => {
  const showToast = useAppToast();
  const {route} = props;
  const id = route?.params?.postId;
  const {
    data: postData,
    error: postError,
    loading: postLoading,
    refetch,
  } = usePost({
    variables: {
      input: {
        id,
      },
    },
  });

  const post = postData?.post;
  const correctDate = new Date(post?.createdAt ?? '');
  const image = post?.mediaUrl ? post?.mediaUrl : images.noImage;

  useEffect(() => {
    if (postError) {
      showToast(postError.message, 'danger');
    }
  }, [postError, showToast]);

  useFocusEffect(
    useCallback(() => {
      refetch();
    }, [refetch]),
  );

  return (
    <DefaultLayout flex="start" topPanelTitle={post?.title}>
      {postLoading ? (
        <LoaderEmptyContent />
      ) : (
        <>
          <Text style={styles.date}>
            {returnString.getPrettyDate(correctDate)}
          </Text>

          <View style={styles.imageWrapper}>
            <Image
              style={styles.image}
              source={image ? {uri: image} : images.exampleAvatar}
            />
          </View>

          <DefaultText
            style={styles.description}
            text={post?.description ?? ''}
            font="Body6_Regular_14pt"
          />

          <PostBottom
            author={`${post?.author.firstName} ${
              post?.author.lastName ? post?.author.lastName[0] : ''
            }`}
            isLiked={post?.isLiked ?? false}
            likesCount={post?.likesCount ?? 0}
            postId={post?.id ?? ''}
            avatar={post?.author.avatarUrl ?? ''}
          />
        </>
      )}
    </DefaultLayout>
  );
};

const styles = StyleSheet.create({
  date: {
    alignSelf: 'center',
    marginBottom: 8,
    color: colors.dark.grayscale[300],
    ...typography.Body6_Regular_14pt,
  },
  imageWrapper: {
    height: 226,
    width: '100%',
    marginBottom: 20,
    borderRadius: 17,
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    borderRadius: 17,
  },
  description: {
    marginBottom: 20,
  },
});
