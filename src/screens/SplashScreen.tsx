import React, {useContext} from 'react';
import {ActivityIndicator} from 'react-native';
import {LinkingOptions, NavigationContainer} from '@react-navigation/native';
import RNBootSplash from 'react-native-bootsplash';
import {useToken} from '../hooks/useToken';
import {ThemeContext} from '../context/ThemeContext';
import {colors} from '../assets/styles/colors';
import {Authorized} from './Authorized';
import {Unauthorized} from './Unauthorized';

const linking: LinkingOptions<any> = {
  prefixes: ['socialrnapp://'],
  config: {
    screens: {
      DRAWER: {
        screens: {
          MAIN: {
            path: 'home',
          },
          FAVORITES: {
            path: 'favorites',
          },
          POSTS: {
            path: 'myposts',
          },
          POST: {
            path: 'post/:postId',
          },
        },
      },
    },
  },
};

export const SplashScreen = () => {
  const {token} = useToken();
  const {isDarkMode} = useContext(ThemeContext);

  return (
    <NavigationContainer
      onReady={() => RNBootSplash.hide({fade: true})}
      linking={linking}
      fallback={
        <ActivityIndicator
          size="large"
          color={
            isDarkMode
              ? colors.dark.primary.default
              : colors.light.primary.default
          }
        />
      }>
      {token ? <Authorized /> : <Unauthorized />}
    </NavigationContainer>
  );
};
