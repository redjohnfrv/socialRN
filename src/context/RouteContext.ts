import {createContext} from 'react';
import {RouteContextType} from '../types';
import {ROUTES} from '../constants/routes';

export const RouteContext = createContext<RouteContextType>({
  prevRoute: ROUTES.TAB_ROUTE_MAIN,
  onRouteChange: () => {},
});
