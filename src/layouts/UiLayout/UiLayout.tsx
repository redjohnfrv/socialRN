import React, {ReactNode} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {useThemeColors} from '../../hooks/useThemeColors';
import {typography} from '../../assets/styles/typography';

type Props = {
  title: string;
  children: ReactNode;
};

export const UiLayout = (props: Props) => {
  const {title, children} = props;
  const {modeColor, modeColorInverse} = useThemeColors();
  const navigation = useNavigation<NativeStackNavigationProp<any>>();

  return (
    <SafeAreaView
      style={[
        styles.wrapper,
        {backgroundColor: modeColorInverse.grayscale[700]},
      ]}>
      <ScrollView>
        <Text style={[styles.subTitle, {color: modeColor.grayscale[700]}]}>
          {title}
        </Text>

        {children}

        <TouchableOpacity
          style={{marginTop: 50}}
          onPress={() => navigation.goBack()}>
          <Text style={{color: modeColor.grayscale[700]}}>Back</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingVertical: 32,
    paddingHorizontal: 16,
  },
  title: {
    alignSelf: 'center',
    marginBottom: 32,
    ...typography.Title_1_Regular_55pt,
  },
  subTitle: {
    alignSelf: 'center',
    marginBottom: 16,
    ...typography.Title_3_SemiBold_32pt,
  },
});
