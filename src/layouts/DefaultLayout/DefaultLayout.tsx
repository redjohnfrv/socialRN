import React, {ElementType, ReactNode} from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {useThemeColors} from '../../hooks/useThemeColors';
import {TopNavigationPanel} from '../../components';

type FlexJustifyType = 'start' | 'end';

type Props = {
  children: ReactNode;
  paddings?: number;
  topPanelTitle?: string;
  flex?: FlexJustifyType;
  RightButton?: ElementType;
  onClickBackEffect?: () => void;
};

export const DefaultLayout = (props: Props) => {
  const {
    children,
    paddings = 16,
    flex = 'end',
    topPanelTitle = '',
    RightButton,
    onClickBackEffect,
  } = props;
  const {modeColor} = useThemeColors();

  const layoutCustomStyles = {
    backgroundColor: modeColor.grayscale[100],
    justifyContent: flex === 'end' ? 'flex-end' : 'flex-start',
  } as const;

  return (
    <SafeAreaView
      style={[
        styles.root,
        {...layoutCustomStyles},
        {paddingHorizontal: paddings},
      ]}>
      {topPanelTitle && (
        <TopNavigationPanel
          title={topPanelTitle}
          RightButton={RightButton}
          onClickBackEffect={onClickBackEffect}
        />
      )}
      {children}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    position: 'relative',
    flex: 1,
    flexDirection: 'column',
  },
});
