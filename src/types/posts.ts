export type PostCustomType = {
  id: string;
  authorId: string;
  createdAt: string;
  isLiked: boolean;
  likesCount: number;
  mediaUrl: string;
  title: string;
  description: string;
  author: {
    avatarUrl?: string | null;
    firstName?: string | null;
    lastName?: string | null;
  };
};

export type CreatePostType = {
  mediaUrl: string | null;
  description: string;
  title: string;
};
