export type ProfileFormType = {
  avatarUrl: string | null;
  birthDate: Date;
  country: string;
  email: string;
  firstName: string;
  gender: string;
  id: string;
  lastName: string;
  middleName: string;
  phone: string;
};
