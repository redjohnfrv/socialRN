import {ROUTES} from '../constants/routes';

export enum TabBarIconVariant {
  MAIN = 'main',
  FAVORITES = 'favorites',
  POSTS = 'posts',
}

export enum UploadImageCategory {
  AVATARS = 'AVATARS',
  POSTS = 'POSTS',
}

export type ThemeContextType = {
  isDarkMode: boolean;
  onThemeToggle: () => void;
};

export type RouteContextType = {
  prevRoute: ROUTES;
  onRouteChange: (route: ROUTES) => void;
};

export type EyeVariant = 'unable' | 'active' | 'success' | 'error';

export type CopyLinkButtonVariant = 'white' | 'black' | 'gray' | 'none';

export type MenuIconVariant = 'profile' | 'exit' | 'mode';
