import bg from '../splash.png';
import avatarLight from './avatarLight.png';
import avatarDark from './avatarDark.png';
import noImage from './noImage.png';

import examplePost from './examplePost.jpeg';
import exampleAvatar from './exampleAvatar.png';

export const images = {
  bg,
  avatarLight,
  avatarDark,
  noImage,

  exampleAvatar,
  examplePost,
};
