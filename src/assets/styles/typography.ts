import {StyleSheet} from 'react-native';

export const typography: Record<string, any> = StyleSheet.create({
  Body1_Medium_18pt: {
    fontFamily: 'Outfit-Medium',
    fontSize: 18,
    lineHeight: 23,
    fontWeight: '500',
  },
  Body2_Medium_16pt: {
    fontFamily: 'Outfit-Medium',
    fontSize: 16,
    lineHeight: 20,
    fontWeight: '500',
  },
  Body3_Medium_14pt: {
    fontFamily: 'Outfit-Medium',
    fontSize: 14,
    lineHeight: 18,
    fontWeight: '500',
  },
  Body4_Regular_18pt: {
    fontFamily: 'Outfit-Regular',
    fontSize: 18,
    lineHeight: 23,
  },
  Body5_Regular_16pt: {
    fontFamily: 'Outfit-Regular',
    fontSize: 16,
    lineHeight: 20,
    letterSpacing: 0.03,
  },
  Body6_Regular_14pt: {
    fontFamily: 'Outfit-Regular',
    fontSize: 14,
    lineHeight: 19,
  },
  Title_2_Medium_32pt: {
    fontFamily: 'Outfit-Medium',
    fontSize: 32,
    lineHeight: 40,
    fontWeight: '500',
  },
  Title_3_SemiBold_32pt: {
    fontFamily: 'Outfit-SemiBold',
    fontSize: 32,
    lineHeight: 40,
    fontWeight: '400',
  },
  Title_4_SemiBold_20pt: {
    fontFamily: 'Outfit-SemiBold',
    fontSize: 20,
    lineHeight: 25,
    fontWeight: '400',
  },
  Headline_1_SemiBold_18pt: {
    fontFamily: 'Outfit-SemiBold',
    fontSize: 18,
    lineHeight: 23,
    fontWeight: '600',
  },
  Headline_2_SemiBold_14pt: {
    fontFamily: 'Outfit-SemiBold',
    fontSize: 14,
    lineHeight: 18,
    fontWeight: '600',
  },
  Caption1_Regular_12pt: {
    fontFamily: 'Outfit-Medium',
    fontSize: 12,
    lineHeight: 15,
    fontWeight: '500',
  },
  Title_1_Regular_55pt: {
    fontFamily: 'Nokwy',
    fontSize: 55,
    lineHeight: 53,
  },
});
