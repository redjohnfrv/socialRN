export const colors = {
  light: {
    grayscale: {
      100: '#FFF',
      150: '#F4F5F4',
      200: '#DEDEDE',
      250: '#D0D1D0',
      300: '#9B9B9B',
      400: '#696969',
      700: '#131313',
    },
    primary: {
      default: '#B8DE64',
      pressed: '#75C537',
    },
  },
  dark: {
    grayscale: {
      700: '#FFF',
      400: '#DEDEDE',
      300: '#9B9B9B',
      250: '#696969',
      200: '#303030',
      150: '#1B1B1B',
      100: '#131313',
    },
    primary: {
      default: '#87B71F',
      pressed: '#618909',
    },
  },
  additional: {
    error: '#C2534C',
    iosBlur: {
      background: '#000',
      backdropFilter: 'blur(40px)',
    },
  },
};
