import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {ROUTES} from '../../constants/routes';
import {
  FavoritesScreen,
  MainScreen,
  MyPostsScreen,
  PostScreen,
  CreatePostScreen,
} from '../../screens/Authorized/screens';
import {TabBar} from '../../components';

const Tab = createBottomTabNavigator();

export const BottomTabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={{headerShown: false}}
      backBehavior="none"
      tabBar={props => <TabBar {...props} />}>
      <Tab.Screen name={ROUTES.TAB_ROUTE_MAIN} component={MainScreen} />
      <Tab.Screen
        name={ROUTES.TAB_ROUTE_FAVORITES}
        component={FavoritesScreen}
      />
      <Tab.Screen name={ROUTES.TAB_ROUTE_POSTS} component={MyPostsScreen} />
      <Tab.Screen
        name={ROUTES.ROUTE_POST}
        component={PostScreen}
        initialParams={{postId: '0'}}
      />
      <Tab.Screen
        name={ROUTES.ROUTE_CREATE_POST}
        component={CreatePostScreen}
      />
    </Tab.Navigator>
  );
};
