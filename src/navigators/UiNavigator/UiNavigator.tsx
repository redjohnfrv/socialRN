import React from 'react';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import {
  createNativeStackNavigator,
  NativeStackNavigationProp,
} from '@react-navigation/native-stack';
import {useNavigation} from '@react-navigation/native';
import {useThemeColors} from '../../hooks/useThemeColors';
import {ROUTES} from '../../constants/routes';
import {typography} from '../../assets/styles/typography';
import {
  AvatarUI,
  ButtonsUI,
  ButtonWithIconUI,
  CopyLinkButtonUI,
  IconButtonsUI,
  InputUI,
  SecureInputUI,
  TabsUI,
  TextButtonUI,
  RadioButtonsUI,
  ImageUploadUI,
} from '../../components/UI/components';

const Stack = createNativeStackNavigator();

type DataListType = {
  id: number;
  title: string;
  link: ROUTES;
};

const dataList = [
  {
    id: 1,
    title: 'BUTTONS',
    link: ROUTES.UI_ROUTE_BUTTONS,
  },
  {
    id: 2,
    title: 'COPY LINK',
    link: ROUTES.UI_ROUTE_COPY_LINK,
  },
  {
    id: 3,
    title: 'TEXT BUTTON',
    link: ROUTES.UI_ROUTE_TEXT_BUTTON,
  },
  {
    id: 4,
    title: 'DEFAULT INPUT',
    link: ROUTES.UI_ROUTE_INPUT,
  },
  {
    id: 5,
    title: 'SECURE INPUT',
    link: ROUTES.UI_ROUTE_SECURE_INPUT,
  },
  {
    id: 6,
    title: 'TABS',
    link: ROUTES.UI_ROUTE_TABS,
  },
  {
    id: 7,
    title: 'BUTTONS WITH ICON',
    link: ROUTES.UI_ROUTE_BUTTONS_WITH_ICON,
  },
  {
    id: 8,
    title: 'ICON BUTTONS',
    link: ROUTES.UI_ROUTE_ICON_BUTTONS,
  },
  {
    id: 9,
    title: 'AVATAR',
    link: ROUTES.UI_ROUTE_AVATAR,
  },
  {
    id: 10,
    title: 'RADIO BUTTON',
    link: ROUTES.UI_ROUTE_RADIO_BUTTONS,
  },
  {
    id: 11,
    title: 'IMAGE UPLOADER',
    link: ROUTES.UI_ROUTE_IMAGE_UPLOAD,
  },
];

const Item = (props: DataListType) => {
  const {title, link} = props;
  const {modeColor} = useThemeColors();
  const navigation = useNavigation<NativeStackNavigationProp<any>>();

  const onPress = (routeLink: ROUTES) => {
    navigation.navigate(routeLink);
  };

  return (
    <TouchableOpacity style={{marginBottom: 20}} onPress={() => onPress(link)}>
      <Text style={{color: modeColor.grayscale[700]}}>{title}</Text>
    </TouchableOpacity>
  );
};

const List = () => {
  const {modeColor, modeColorInverse} = useThemeColors();
  const navigation = useNavigation<NativeStackNavigationProp<any>>();

  return (
    <SafeAreaView
      style={[
        styles.wrapper,
        {backgroundColor: modeColorInverse.grayscale[700]},
      ]}>
      <Text style={[styles.title, {color: modeColor.grayscale[700]}]}>UI</Text>

      <FlatList
        data={dataList}
        renderItem={({item}) => (
          <Item title={item.title} link={item.link} id={item.id} />
        )}
        keyExtractor={item => String(item.id)}
      />

      <TouchableOpacity
        style={{marginTop: 50}}
        onPress={() => navigation.goBack()}>
        <Text style={{color: modeColor.grayscale[700]}}>Back</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export const UiNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName={ROUTES.UI_ROUTE_LIST}
      screenOptions={{headerShown: false}}>
      <Stack.Screen name={ROUTES.UI_ROUTE_LIST} component={List} />
      <Stack.Screen name={ROUTES.UI_ROUTE_BUTTONS} component={ButtonsUI} />
      <Stack.Screen
        name={ROUTES.UI_ROUTE_COPY_LINK}
        component={CopyLinkButtonUI}
      />
      <Stack.Screen
        name={ROUTES.UI_ROUTE_TEXT_BUTTON}
        component={TextButtonUI}
      />
      <Stack.Screen name={ROUTES.UI_ROUTE_INPUT} component={InputUI} />
      <Stack.Screen
        name={ROUTES.UI_ROUTE_SECURE_INPUT}
        component={SecureInputUI}
      />
      <Stack.Screen name={ROUTES.UI_ROUTE_TABS} component={TabsUI} />
      <Stack.Screen
        name={ROUTES.UI_ROUTE_BUTTONS_WITH_ICON}
        component={ButtonWithIconUI}
      />
      <Stack.Screen name={ROUTES.UI_ROUTE_AVATAR} component={AvatarUI} />
      <Stack.Screen
        name={ROUTES.UI_ROUTE_ICON_BUTTONS}
        component={IconButtonsUI}
      />
      <Stack.Screen
        name={ROUTES.UI_ROUTE_RADIO_BUTTONS}
        component={RadioButtonsUI}
      />
      <Stack.Screen
        name={ROUTES.UI_ROUTE_IMAGE_UPLOAD}
        component={ImageUploadUI}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingVertical: 32,
    paddingHorizontal: 16,
  },
  title: {
    alignSelf: 'center',
    marginBottom: 32,
    ...typography.Title_1_Regular_55pt,
  },
});
