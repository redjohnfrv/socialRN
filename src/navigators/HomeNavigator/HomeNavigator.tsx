import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {ROUTES} from '../../constants/routes';
import {BottomTabNavigator} from '../BottomTabNavigator';
import {ProfileStackNavigator} from '../ProfileStackNavigator';
import {SideMenu} from '../../components';

const Drawer = createDrawerNavigator();

export const HomeNavigator = () => {
  return (
    <Drawer.Navigator
      backBehavior="none"
      drawerContent={props => <SideMenu {...props} />}
      screenOptions={{headerShown: false}}>
      <Drawer.Screen
        name={ROUTES.DRAWER_ROUTE}
        component={BottomTabNavigator}
      />
      <Drawer.Screen
        name={ROUTES.DRAWER_ROUTE_PROFILE}
        component={ProfileStackNavigator}
      />
    </Drawer.Navigator>
  );
};
