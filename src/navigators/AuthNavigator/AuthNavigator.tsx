import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {ROUTES} from '../../constants/routes';
import {
  AuthScreen,
  LoginScreen,
  RegisterScreen,
} from '../../screens/Unauthorized/screens';
import {UiNavigator} from '../UiNavigator';

const Stack = createNativeStackNavigator();

export const AuthNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName={ROUTES.ROUTE_AUTH}
      screenOptions={{headerShown: false}}>
      <Stack.Screen name={ROUTES.ROUTE_AUTH} component={AuthScreen} />
      <Stack.Screen name={ROUTES.ROUTE_LOGIN} component={LoginScreen} />
      <Stack.Screen name={ROUTES.ROUTE_REGISTER} component={RegisterScreen} />
      <Stack.Screen name={ROUTES.UI_ROUTE} component={UiNavigator} />
    </Stack.Navigator>
  );
};
