import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {ROUTES} from '../../constants/routes';
import {ProfileScreen} from '../../screens/Authorized/screens';

const Stack = createNativeStackNavigator();

export const ProfileStackNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName={ROUTES.ROUTE_PROFILE}
      screenOptions={{headerShown: false}}>
      <Stack.Screen name={ROUTES.ROUTE_PROFILE} component={ProfileScreen} />
    </Stack.Navigator>
  );
};
