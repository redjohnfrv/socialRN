import React, {useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useThemeColors} from '../../hooks/useThemeColors';
import {typography} from '../../assets/styles/typography';
import {ThemeContext} from '../../context/ThemeContext';
import {colors} from '../../assets/styles/colors';
import {DefaultText} from '../../ui';

type Props = {
  text: string;
};

export const NoContent = (props: Props) => {
  const {text} = props;
  const {modeColor} = useThemeColors();
  const {isDarkMode} = useContext(ThemeContext);

  const textStyles = isDarkMode
    ? colors.dark.grayscale[250]
    : colors.dark.grayscale[100];
  return (
    <View style={styles.root}>
      <View
        style={[styles.title, {backgroundColor: modeColor.primary.default}]}>
        <DefaultText text="UPS" font="Title_1_Regular_55pt" />
      </View>

      <Text style={[styles.text, {color: textStyles}]}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    paddingVertical: 13,
    paddingHorizontal: 21,
    marginBottom: 24,
    transform: [
      {
        rotate: '-3deg',
      },
    ],
  },
  text: {
    width: 200,
    textAlign: 'center',
    ...typography.Body5_Regular_16pt,
  },
});
