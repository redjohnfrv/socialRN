import React from 'react';
import {StyleSheet, View} from 'react-native';
import {DefaultAvatar, DefaultText} from '../../../../ui';

type Props = {
  name: string;
  avatarUrl: string;
};

export const Profile = (props: Props) => {
  const {name, avatarUrl} = props;

  return (
    <View>
      <View style={styles.avatarWrapper}>
        <DefaultAvatar
          variant="medium"
          src={avatarUrl ? {uri: avatarUrl} : ''}
        />
      </View>
      <DefaultText
        text={name}
        font="Title_4_SemiBold_20pt"
        numberOfLines={1}
        style={styles.name}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  avatarWrapper: {
    marginBottom: 12,
  },
  name: {
    paddingRight: 4,
  },
});
