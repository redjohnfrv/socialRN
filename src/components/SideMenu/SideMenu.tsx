import React, {useContext} from 'react';
import {DrawerContentScrollView} from '@react-navigation/drawer';
import {StyleSheet, View} from 'react-native';
import {useThemeColors} from '../../hooks/useThemeColors';
import {useToken} from '../../hooks/useToken';
import {useUserMe} from '../../graphql/queries/__generated__/userMe.query';
import {ThemeContext} from '../../context/ThemeContext';
import {LOGOUT} from '../../constants/common';
import {ROUTES} from '../../constants/routes';
import {DefaultButtonWithIcon} from '../../ui';
import {Profile} from './components';

export const SideMenu = (props: any) => {
  const {modeColor} = useThemeColors();
  const {isDarkMode, onThemeToggle} = useContext(ThemeContext);
  const {data} = useUserMe();
  const {onSetAuth} = useToken();
  const navigation = props.navigation;

  const getUnAuth = async () => {
    await onSetAuth(LOGOUT);
  };

  const profile = data?.userMe;
  const userName = `${profile?.firstName ?? ''} ${profile?.lastName ?? ''}`;
  const avatarUrl = data?.userMe.avatarUrl ?? '';

  return (
    <DrawerContentScrollView
      style={[
        styles.root,
        {
          backgroundColor: modeColor.grayscale[100],
        },
      ]}
      {...props}>
      <View>
        <View style={styles.profileWrapper}>
          <Profile name={userName} avatarUrl={avatarUrl} />
        </View>

        <View style={styles.profileButtonWrapper}>
          <DefaultButtonWithIcon
            text="Profile"
            iconVariant="profile"
            onPress={() => navigation.navigate(ROUTES.DRAWER_ROUTE_PROFILE)}
          />
        </View>

        <View>
          <DefaultButtonWithIcon
            text="Exit"
            iconVariant="exit"
            onPress={getUnAuth}
          />
        </View>
      </View>

      <View style={styles.modeWrapper}>
        <DefaultButtonWithIcon
          text={isDarkMode ? 'Night theme' : 'Light theme'}
          iconVariant="mode"
          onPress={onThemeToggle}
        />
      </View>
    </DrawerContentScrollView>
  );
};

const styles = StyleSheet.create({
  root: {
    position: 'relative',
    paddingLeft: 32,
    paddingTop: 80,
    paddingBottom: 40,
  },
  profileWrapper: {
    marginBottom: 60,
  },
  profileButtonWrapper: {
    marginBottom: 33,
  },
  modeWrapper: {
    marginTop: '140%',
  },
});
