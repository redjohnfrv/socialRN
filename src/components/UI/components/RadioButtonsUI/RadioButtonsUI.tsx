import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useThemeColors} from '../../../../hooks/useThemeColors';
import {typography} from '../../../../assets/styles/typography';
import {UiLayout} from '../../../../layouts';
import {DefaultRadioButtons} from '../../../../ui';

export const RadioButtonsUI = () => {
  const {modeColor} = useThemeColors();
  const [value, setValue] = useState('Male');

  return (
    <UiLayout title="RADIO BUTTONS">
      <View style={styles.buttonWrapper}>
        <DefaultRadioButtons
          firstValue="Male"
          secondValue="Female"
          onChange={() =>
            value === 'Male' ? setValue('Female') : setValue('Male')
          }
          value={value}
        />
      </View>

      <Text style={[styles.indent, {color: modeColor.grayscale[700]}]}>
        Value is: {value.toUpperCase()}
      </Text>
    </UiLayout>
  );
};

const styles = StyleSheet.create({
  buttonWrapper: {
    flexDirection: 'row',
    marginBottom: 24,
  },
  indent: {
    marginTop: 16,
    ...typography.Body2_Medium_16pt,
  },
});
