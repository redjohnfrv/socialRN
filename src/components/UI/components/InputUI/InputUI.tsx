import React from 'react';
import {StyleSheet, View} from 'react-native';
import {DefaultInput} from '../../../../ui';
import {UiLayout} from '../../../../layouts';

export const InputUI = () => {
  return (
    <UiLayout title="INPUTS">
      <View>
        <DefaultInput
          placeholder="E-mail"
          title="E-mail"
          customStyles={styles.indent}
        />

        <DefaultInput
          placeholder="Enter some text"
          customStyles={styles.indent}
        />

        <DefaultInput
          placeholder="Enter some success text"
          customStyles={styles.indent}
          success={true}
        />

        <DefaultInput
          title="Input title"
          placeholder="Enter some error text"
          customStyles={styles.indent}
          error="Something goes wrong something goes wrong something goes wrong something goes wrong something goes wrong something goes wrong"
        />

        <DefaultInput
          placeholder="Disabled"
          customStyles={styles.indent}
          enabled={false}
        />
      </View>
    </UiLayout>
  );
};

const styles = StyleSheet.create({
  indent: {
    marginBottom: 24,
  },
});
