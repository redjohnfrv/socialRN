import React, {useCallback, useState} from 'react';
import {useGetPictureWithPermission} from '../../../../hooks/useGetPictureWithPermission';
import {ImagePickerData} from '../../../../utils/uploadService';
import {UiLayout} from '../../../../layouts';
import {ImagePickerModal, UploadImageArea} from '../../../../ui';
import {useSwitcher} from '../../../../hooks/useSwitcher';

export const ImageUploadUI = () => {
  const [uri, setUri] = useState<string | undefined>(undefined);
  const [, setImageData] = useState<ImagePickerData>(undefined);
  const {getImage} = useGetPictureWithPermission();
  const {
    isOn: isVisible,
    off: setVisibleOff,
    on: setVisibleOn,
  } = useSwitcher(false);

  const onSetImageState = useCallback(
    async (isCamera?: boolean) => {
      const image = await getImage(isCamera);
      setImageData(image);

      if (image?.data) {
        setUri(image?.source.uri || '');
        setVisibleOff();
      }
    },
    [getImage, setVisibleOff],
  );

  const onClearImageState = () => {
    setUri(undefined);
    setImageData(undefined);
    setVisibleOff();
  };

  const onImageLibraryPress = useCallback(() => {
    onSetImageState(false);
  }, [onSetImageState]);

  const onCameraPress = useCallback(() => {
    onSetImageState(true);
  }, [onSetImageState]);

  return (
    <UiLayout title="Image upload">
      <UploadImageArea uri={uri} onPress={() => setVisibleOn()} />

      <ImagePickerModal
        isVisible={isVisible}
        onClose={() => setVisibleOff()}
        onImageLibraryPress={onImageLibraryPress}
        onCameraPress={onCameraPress}
        onDeleteImage={onClearImageState}
      />
    </UiLayout>
  );
};
