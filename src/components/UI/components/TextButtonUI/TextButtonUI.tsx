import React from 'react';
import {StyleSheet, View} from 'react-native';
import {DefaultTextButton} from '../../../../ui';
import {UiLayout} from '../../../../layouts';

export const TextButtonUI = () => {
  return (
    <UiLayout title="TEXT BUTTON">
      <View>
        <DefaultTextButton title="Default" customStyles={styles.buttonIndent} />

        <DefaultTextButton
          title="Disabled"
          customStyles={styles.buttonIndent}
          disabled={true}
        />

        <DefaultTextButton
          title="Loading"
          customStyles={styles.buttonIndent}
          isLoading={true}
        />
      </View>
    </UiLayout>
  );
};

const styles = StyleSheet.create({
  buttonIndent: {
    marginBottom: 12,
  },
});
