import React from 'react';
import {StyleSheet, View} from 'react-native';
import {UiLayout} from '../../../../layouts';
import {DefaultButtonWithIcon} from '../../../../ui';

export const ButtonWithIconUI = () => {
  return (
    <UiLayout title="BUTTONS WITH ICON">
      <View style={styles.indent}>
        <DefaultButtonWithIcon
          text="Profile"
          iconVariant="profile"
          onPress={() => {}}
        />
      </View>

      <View style={styles.indent}>
        <DefaultButtonWithIcon
          text="Exit"
          iconVariant="exit"
          onPress={() => {}}
        />
      </View>

      <View style={styles.indent}>
        <DefaultButtonWithIcon
          text="Dark mode"
          iconVariant="mode"
          onPress={() => {}}
        />
      </View>
    </UiLayout>
  );
};

const styles = StyleSheet.create({
  indent: {
    marginBottom: 12,
  },
});
