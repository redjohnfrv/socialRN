import React from 'react';
import {ScrollView, View} from 'react-native';
import {images} from '../../../../assets/images';
import {DefaultAvatar} from '../../../../ui';
import {UiLayout} from '../../../../layouts';

export const AvatarUI = () => {
  return (
    <UiLayout title="AVATAR">
      <ScrollView>
        <DefaultAvatar variant="large" />
        <View style={{width: 1, height: 32}} />
        <DefaultAvatar variant="medium" />
        <View style={{width: 1, height: 32}} />
        <DefaultAvatar variant="small" />
        <View style={{width: 1, height: 32}} />

        <DefaultAvatar variant="large" src={images.exampleAvatar} />
        <View style={{width: 1, height: 32}} />
        <DefaultAvatar variant="medium" src={images.exampleAvatar} />
        <View style={{width: 1, height: 32}} />
        <DefaultAvatar variant="small" src={images.exampleAvatar} />
      </ScrollView>
    </UiLayout>
  );
};
