import React from 'react';
import {StyleSheet, View} from 'react-native';
import {DefaultButton} from '../../../../ui';
import {UiLayout} from '../../../../layouts';

export const ButtonsUI = () => {
  return (
    <UiLayout title="BUTTONS">
      <View>
        <View>
          <DefaultButton
            customStyles={styles.indent}
            title="Continue"
            type="large"
          />
          <DefaultButton
            customStyles={styles.indent}
            title="Continue"
            type="medium"
          />
          <DefaultButton
            customStyles={styles.indent}
            title="Continue"
            type="small"
          />
        </View>

        <View>
          <DefaultButton
            customStyles={styles.indent}
            title="Continue"
            type="large"
            disabled={true}
          />
          <DefaultButton
            customStyles={styles.indent}
            title="Continue"
            type="small"
            isLoading={true}
          />
        </View>
      </View>
    </UiLayout>
  );
};

const styles = StyleSheet.create({
  indent: {
    marginBottom: 12,
  },
});
