import React from 'react';
import {StyleSheet, View} from 'react-native';
import {SecureInput} from '../../../../ui';
import {UiLayout} from '../../../../layouts';

export const SecureInputUI = () => {
  return (
    <UiLayout title="SECURE INPUT">
      <View>
        {/*@ts-ignore*/}
        <SecureInput
          title="Password"
          placeholder="Enter your password"
          customStyles={styles.indent}
        />

        {/*@ts-ignore*/}
        <SecureInput
          title="Password"
          placeholder="Enter your password"
          customStyles={styles.indent}
          enabled={false}
        />

        {/*@ts-ignore*/}
        <SecureInput
          title="Password"
          placeholder="Entered success password"
          customStyles={styles.indent}
          success={true}
        />

        {/*@ts-ignore*/}
        <SecureInput
          title="Password"
          placeholder="Confirm your password"
          customStyles={styles.indent}
          error="Wrong password"
        />
      </View>
    </UiLayout>
  );
};

const styles = StyleSheet.create({
  indent: {
    marginBottom: 24,
  },
});
