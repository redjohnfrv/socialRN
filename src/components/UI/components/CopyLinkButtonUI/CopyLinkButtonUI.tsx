import React from 'react';
import {StyleSheet, View} from 'react-native';
import {CopyLinkButton} from '../../../../ui';
import {UiLayout} from '../../../../layouts';

export const CopyLinkButtonUI = () => {
  return (
    <UiLayout title="COPY LINK BUTTON">
      <View>
        <CopyLinkButton customStyles={styles.buttonIndent} />
        <CopyLinkButton customStyles={styles.buttonIndent} disabled={true} />
        <CopyLinkButton customStyles={styles.buttonIndent} isLoading={true} />
      </View>
    </UiLayout>
  );
};

const styles = StyleSheet.create({
  buttonIndent: {
    marginBottom: 12,
  },
});
