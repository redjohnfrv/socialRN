import React from 'react';
import {StyleSheet, View} from 'react-native';
import {UiLayout} from '../../../../layouts';
import {AddPostButton, AvatarUploadButton} from '../../../../ui';
import {HeartIcon, ImageIcon, SharedIcon} from '../../../../ui/ComponentsSvg';

export const IconButtonsUI = () => {
  return (
    <UiLayout title="ICON BUTTONS">
      <View style={styles.buttonWrapper}>
        <View style={styles.button}>
          <AvatarUploadButton />
        </View>
        <View style={styles.button}>
          <AvatarUploadButton disabled={true} />
        </View>
      </View>

      <View style={styles.buttonWrapper}>
        <View style={styles.button}>
          <HeartIcon />
        </View>
        <View style={styles.button}>
          <HeartIcon active={true} />
        </View>
      </View>

      <View style={styles.buttonWrapper}>
        <View style={styles.button}>
          <ImageIcon />
        </View>
        <View style={styles.button}>
          <ImageIcon active={true} />
        </View>
      </View>

      <View style={styles.buttonWrapper}>
        <View style={styles.button}>
          <SharedIcon />
        </View>
        <View style={styles.button}>
          <SharedIcon active={true} />
        </View>
      </View>

      <View style={styles.buttonWrapper}>
        <View style={styles.button}>
          <AddPostButton />
        </View>
        <View style={styles.button}>
          <AddPostButton disabled={true} />
        </View>
      </View>
    </UiLayout>
  );
};

const styles = StyleSheet.create({
  buttonWrapper: {
    flexDirection: 'row',
    marginBottom: 24,
  },
  button: {
    marginRight: 12,
  },
});
