import React, {useState} from 'react';
import {View} from 'react-native';
import {PostFilterType} from '../../../../__generated__/types';
import {Tabs} from '../../../../ui';
import {UiLayout} from '../../../../layouts';

export const TabsUI = () => {
  const [postsFilter, setPostsFilter] = useState(PostFilterType.New);

  return (
    <UiLayout title="TABS">
      <View>
        <Tabs onPress={setPostsFilter} tab={postsFilter} />
      </View>
    </UiLayout>
  );
};
