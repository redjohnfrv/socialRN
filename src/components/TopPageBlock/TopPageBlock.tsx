import React from 'react';
import {StyleSheet, View} from 'react-native';
import {DefaultAvatar, DefaultText} from '../../ui';

type Props = {
  title: string;
  avatarUrl: string | undefined;
};

export const TopPageBlock = (props: Props) => {
  const {avatarUrl, title} = props;

  return (
    <View style={styles.root}>
      <DefaultText text={title} font="Title_2_Medium_32pt" numberOfLines={1} />
      <DefaultAvatar src={avatarUrl ? {uri: avatarUrl} : ''} variant="small" />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 60,
    marginBottom: 20,
  },
});
