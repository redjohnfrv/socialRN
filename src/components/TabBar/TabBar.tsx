import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {BottomTabBarProps} from '@react-navigation/bottom-tabs';
import {useThemeColors} from '../../hooks/useThemeColors';
import {ROUTES, tabBarRoutesAdapter} from '../../constants/routes';
import {RouteContext} from '../../context/RouteContext';
import {TabBarIconVariant} from '../../types';
import {TabBarButton} from '../../ui';
import {TabBarIcon} from '../../ui/ComponentsSvg';

const tabRouteProps = [
  {
    id: 1,
    title: 'Main',
    route: ROUTES.TAB_ROUTE_MAIN,
    variant: TabBarIconVariant.MAIN,
  },
  {
    id: 2,
    title: 'Favorites',
    route: ROUTES.TAB_ROUTE_FAVORITES,
    variant: TabBarIconVariant.FAVORITES,
  },
  {
    id: 3,
    title: 'My Posts',
    route: ROUTES.TAB_ROUTE_POSTS,
    variant: TabBarIconVariant.POSTS,
  },
];

export const TabBar = (props: BottomTabBarProps) => {
  const {navigation} = props;
  const {prevRoute} = useContext(RouteContext);
  const {modeColor} = useThemeColors();
  const [activeTab, setActiveTab] = useState<string>(TabBarIconVariant.MAIN);

  const onButtonPress = (tabTitle: TabBarIconVariant, route: ROUTES) => {
    setActiveTab(tabTitle);
    navigation.navigate(route);
  };

  useEffect(() => {
    setActiveTab(tabBarRoutesAdapter[prevRoute]);
  }, [prevRoute]);

  return (
    <View style={[styles.root, {backgroundColor: modeColor.grayscale[100]}]}>
      {tabRouteProps.map(tabRoute => (
        <TabBarButton
          onPress={() => onButtonPress(tabRoute.variant, tabRoute.route)}
          key={tabRoute.id}
          title={tabRoute.title}
          active={tabRoute.variant === activeTab}
          Icon={() => (
            <TabBarIcon
              variant={tabRoute.variant as TabBarIconVariant}
              active={tabRoute.variant === activeTab}
            />
          )}
        />
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    paddingVertical: 21,
  },
});
