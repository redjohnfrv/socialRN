import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {useNavigation} from '@react-navigation/native';
import {typography} from '../../../../assets/styles/typography';
import {colors} from '../../../../assets/styles/colors';
import {returnString} from '../../../../utils';
import {ROUTES} from '../../../../constants/routes';
import {DefaultText} from '../../../../ui';

type Props = {
  title: string;
  date: string;
  postId: string;
};

export const PostHeader = (props: Props) => {
  const {title, date, postId} = props;
  const navigation = useNavigation<NativeStackNavigationProp<any>>();
  const correctDate = returnString.getPrettyDate(new Date(date));

  return (
    <View style={styles.root}>
      <TouchableOpacity
        onPress={() => navigation.navigate(ROUTES.ROUTE_POST, {postId})}>
        <DefaultText
          text={title ?? 'Untitled'}
          font="Body2_Medium_16pt"
          style={styles.title}
        />
      </TouchableOpacity>

      <Text style={styles.date}>{correctDate}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    marginBottom: 13,
  },
  date: {
    color: colors.dark.grayscale[300],
    ...typography.Body6_Regular_14pt,
  },
});
