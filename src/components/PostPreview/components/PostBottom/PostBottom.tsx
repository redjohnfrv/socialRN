import React, {useCallback, useContext, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Share from 'react-native-share';
import {ApolloError} from '@apollo/client';
import {useFocusEffect} from '@react-navigation/native';
import {usePostLike} from '../../../../graphql/mutations/__generated__/postLike.mutation';
import {usePostUnlike} from '../../../../graphql/mutations/__generated__/postUnlike.mutation';
import {useAppToast} from '../../../../hooks/useAppToast';
import {ThemeContext} from '../../../../context/ThemeContext';
import {appUri} from '../../../../constants/api';
import {typography} from '../../../../assets/styles/typography';
import {colors} from '../../../../assets/styles/colors';
import {DefaultAvatar, LikeButton, ShareButton} from '../../../../ui';

type Props = {
  author: string;
  avatar: string;
  isLiked: boolean;
  likesCount: number;
  postId: string;
  postTitle: string;
};

export const PostBottom = (props: Props) => {
  const {author, avatar, isLiked, likesCount, postId, postTitle} = props;
  const {isDarkMode} = useContext(ThemeContext);
  const showToast = useAppToast();
  const [onPostLike, {loading: likeLoading, error: likeError}] = usePostLike();
  const [onPostUnlike, {loading: UnlikeLoading, error: UnlikeError}] =
    usePostUnlike();
  const [likesValueState, setLikesValueState] = useState(likesCount);
  const [isLikedState, setisLikedState] = useState(isLiked);
  const isLikeButtonDisabled = likeLoading || UnlikeLoading;
  const likesCountColor = {
    color: isDarkMode ? colors.dark.grayscale[400] : colors.dark.grayscale[100],
  };

  const onLikeButtonPress = () => {
    if (isLikedState) {
      setLikesValueState(prev => prev - 1);
      setisLikedState(false);
    } else {
      setLikesValueState(prev => prev + 1);
      setisLikedState(true);
    }
  };

  const onPress = () => {
    if (isLikedState) {
      try {
        onPostUnlike({
          variables: {
            input: {
              id: postId,
            },
          },
        });
      } catch (err) {
        if (err instanceof ApolloError) {
          showToast(
            UnlikeError?.message ? UnlikeError.message : err?.message,
            'danger',
          );
        } else {
          showToast('Unknown error. Please connect with developers', 'danger');
        }
      }
    } else {
      try {
        onPostLike({
          variables: {
            input: {
              id: postId,
            },
          },
        });
      } catch (err) {
        if (err instanceof ApolloError) {
          showToast(
            likeError?.message ? likeError.message : err?.message,
            'danger',
          );
        } else {
          showToast('Unknown error. Please connect with developers', 'danger');
        }
      }
    }
  };

  const onShareAction = async () => {
    try {
      await Share.open({
        title: postTitle,
        url: `${appUri}post/${postId}`,
      });
    } catch (err) {
      console.log(err);
    }
  };

  useFocusEffect(
    useCallback(() => {
      setisLikedState(isLiked);
      setLikesValueState(likesCount);
    }, [isLiked, likesCount]),
  );

  return (
    <View style={styles.root}>
      <View style={styles.avatarWrapper}>
        <DefaultAvatar src={{uri: avatar}} variant="small" />

        <Text style={styles.name}>{author}</Text>
      </View>

      <View style={styles.panel}>
        <View style={styles.likes}>
          <LikeButton
            active={isLikedState}
            disabled={isLikeButtonDisabled}
            onLikeButtonPress={onLikeButtonPress}
            onPress={() => onPress()}
          />
          <Text style={[styles.likesCount, likesCountColor]}>
            {likesValueState}
          </Text>
        </View>

        <ShareButton onButtonPress={onShareAction} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  avatarWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  name: {
    marginLeft: 8,
    color: colors.light.grayscale[300],
    ...typography.Body6_Regular_14pt,
  },
  panel: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  likes: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 12,
  },
  likesCount: {
    marginLeft: 10,
    ...typography.Body6_Regular_14pt,
  },
});
