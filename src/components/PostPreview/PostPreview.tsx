import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import {useThemeColors} from '../../hooks/useThemeColors';
import {useSwitcher} from '../../hooks/useSwitcher';
import {PostBottom, PostHeader} from './components';
import {DeleteSwipeButton} from '../../ui';

type Props = {
  id: string;
  title: string;
  src: string;
  date: string;
  isLiked: boolean;
  likesCount: number;
  author: string;
  avatar: string;
  swipeAble?: boolean;
  onPress?: () => void;
  isLoading?: boolean;
};

export const PostPreview = (props: Props) => {
  const {
    title,
    src,
    date,
    likesCount,
    isLiked,
    author,
    id,
    avatar,
    swipeAble = false,
    isLoading = false,
    onPress,
  } = props;
  const {modeColor} = useThemeColors();
  const {
    isOn: isDeleteButtonOpened,
    on: onDeleteButtonReady,
    off: onDeleteButtonUnReady,
  } = useSwitcher();

  const rootStyles = {
    backgroundColor: modeColor.grayscale[150],
  };

  const isDeleteButtonDisabled = !isDeleteButtonOpened || isLoading;

  const DeleteSwipeButtonRenderItem = () => {
    return (
      <DeleteSwipeButton disabled={isDeleteButtonDisabled} onPress={onPress} />
    );
  };

  return (
    <Swipeable
      enabled={swipeAble}
      overshootRight={false}
      renderRightActions={DeleteSwipeButtonRenderItem}
      onSwipeableOpen={() => onDeleteButtonReady()}
      onSwipeableClose={() => onDeleteButtonUnReady()}>
      <View style={[styles.root, rootStyles]}>
        <View style={styles.inner}>
          <PostHeader title={title} date={date} postId={id} />

          <View style={styles.imageWrapper}>
            <Image style={styles.image} source={{uri: src}} />
          </View>

          <PostBottom
            author={author}
            isLiked={isLiked}
            likesCount={likesCount}
            postId={id}
            postTitle={title}
            avatar={avatar}
          />
        </View>
      </View>
    </Swipeable>
  );
};

const styles = StyleSheet.create({
  root: {
    paddingTop: 26,
    paddingBottom: 34,
    marginBottom: 4,
  },
  inner: {
    paddingHorizontal: 20,
  },
  imageWrapper: {
    height: 226,
    width: '100%',
    marginBottom: 20,
  },
  image: {
    flex: 1,
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    borderRadius: 17,
  },
});
