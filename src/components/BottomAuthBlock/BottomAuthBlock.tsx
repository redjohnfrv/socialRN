import React from 'react';
import {StyleSheet, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {useThemeColors} from '../../hooks/useThemeColors';
import {ROUTES} from '../../constants/routes';
import {DefaultButton, DefaultText, DefaultTextButton} from '../../ui';

type Props = {
  linkText?: string;
  linkTitle?: string;
  linkRoute?: ROUTES;
  buttonTitle: string;
  onPressButton: () => void;
  isButtonDisabled?: boolean;
  isLoading?: boolean;
};

export const BottomAuthBlock = (props: Props) => {
  const {
    linkTitle = 'Log in',
    linkRoute = ROUTES.ROUTE_LOGIN,
    linkText = 'Already have an account?',
    buttonTitle,
    onPressButton,
    isButtonDisabled = false,
    isLoading = false,
  } = props;
  const {modeColor} = useThemeColors();
  const navigation = useNavigation<NativeStackNavigationProp<any>>();

  return (
    <View>
      <View style={styles.bottomTextWrapper}>
        <DefaultText
          text={linkText}
          font="Body5_Regular_16pt"
          style={[
            styles.bottomText,
            {
              textShadowColor: modeColor.grayscale[100],
            },
          ]}
        />

        <DefaultTextButton
          title={linkTitle}
          onPress={() => navigation.navigate(linkRoute)}
        />
      </View>

      <View style={styles.buttonWrapper}>
        <DefaultButton
          title={buttonTitle}
          type="large"
          onPress={onPressButton}
          disabled={isButtonDisabled}
          isLoading={isLoading}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  bottomTextWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 32,
  },
  bottomText: {
    marginRight: 4,
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 15,
  },
  buttonWrapper: {
    marginBottom: 50,
  },
});
