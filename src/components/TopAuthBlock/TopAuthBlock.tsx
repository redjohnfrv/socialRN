import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {typography} from '../../assets/styles/typography';
import {useThemeColors} from '../../hooks/useThemeColors';

type Props = {
  title: string;
  text: string;
};

export const TopAuthBlock = (props: Props) => {
  const {title, text} = props;
  const {modeColor} = useThemeColors();

  return (
    <View style={styles.topWrapper}>
      <Text style={[styles.title, {color: modeColor.primary.default}]}>
        {title}
      </Text>
      <Text style={[styles.text, {color: modeColor.grayscale[700]}]}>
        {text}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  topWrapper: {},
  title: {
    ...typography.Title_3_SemiBold_32pt,
  },
  text: {
    ...typography.Body5_Regular_16pt,
  },
});
