import React, {ElementType, useContext} from 'react';
import {StyleSheet, View} from 'react-native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {useNavigation} from '@react-navigation/native';
import {RouteContext} from '../../context/RouteContext';
import {BackButton, DefaultText} from '../../ui';

type Props = {
  title: string;
  RightButton?: ElementType;
  onClickBackEffect?: () => void;
};

const EmptyBlock = () => {
  return <View style={styles.empty} />;
};

export const TopNavigationPanel = (props: Props) => {
  const {title, RightButton, onClickBackEffect} = props;
  const {prevRoute} = useContext(RouteContext);
  const navigation = useNavigation<NativeStackNavigationProp<any>>();

  const onPress = () => {
    onClickBackEffect && onClickBackEffect();
    navigation.navigate(prevRoute);
  };

  return (
    <View style={styles.root}>
      <View style={styles.backButton}>
        <BackButton onPress={onPress} />
      </View>
      <View style={styles.title}>
        <DefaultText
          text={title}
          font="Headline_1_SemiBold_18pt"
          numberOfLines={1}
        />
      </View>
      <View style={styles.rightButton}>
        {RightButton ? <RightButton /> : <EmptyBlock />}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 51,
    paddingBottom: 24,
  },
  title: {
    width: '55%',
    alignItems: 'center',
  },
  empty: {
    width: 20,
    height: 20,
  },
  backButton: {
    width: 80,
  },
  rightButton: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    width: 80,
  },
});
