import React from 'react';
import {StyleSheet, View} from 'react-native';
import {AvatarUploadButton, DefaultAvatar} from '../../ui';

type Props = {
  uri: string | undefined;
  onPress: () => void;
};

export const ProfileAvatar = (props: Props) => {
  const {uri, onPress} = props;

  return (
    <View style={styles.root}>
      <DefaultAvatar variant="large" src={uri ? {uri} : ''} />
      <View style={styles.uploadButton}>
        <AvatarUploadButton onPress={onPress} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
  },
  uploadButton: {
    position: 'absolute',
    bottom: 0,
    transform: [{translateX: 60}],
  },
});
