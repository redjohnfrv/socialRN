import {Alert, Platform} from 'react-native';
import ImagePicker, {Options} from 'react-native-image-crop-picker';
import {
  PERMISSIONS,
  RESULTS,
  check,
  openSettings,
  request,
} from 'react-native-permissions';

const RESIZED_IMAGE_WIDTH = 400;
const RESIZED_IMAGE_HEIGHT = 400;

const IOS_PERMISSIONS_PHOTO_LIBRARY = PERMISSIONS.IOS.PHOTO_LIBRARY;
const IOS_PERMISSIONS_CAMERA = PERMISSIONS.IOS.CAMERA;
const ANDROID_PERMISSIONS = PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE;

const imagePickerOptions: Options = {
  width: RESIZED_IMAGE_WIDTH,
  height: RESIZED_IMAGE_HEIGHT,
  mediaType: 'photo',
  includeBase64: true,
  cropperToolbarTitle: 'Edit your photo',
};

const permissionsRejection = () => {
  Alert.alert(
    'Permission denied',
    'We need access to your photo library in order to upload a photo.',
    [
      {
        text: 'Close',
      },
      {
        text: 'Settings',
        onPress: openSettings,
      },
    ],
    {cancelable: false},
  );
};

export const useGetPictureWithPermission = () => {
  const askPermissionForGettingImage = async (isCamera?: boolean) => {
    const permissions =
      Platform.OS === 'android'
        ? ANDROID_PERMISSIONS
        : isCamera
        ? IOS_PERMISSIONS_CAMERA
        : IOS_PERMISSIONS_PHOTO_LIBRARY;

    const permissionStatus = await check(permissions);
    switch (permissionStatus) {
      case RESULTS.DENIED:
        const res = await request(permissions);
        if (res === RESULTS.GRANTED) {
          return true;
        } else if (res === RESULTS.LIMITED) {
          return true;
        } else {
          console.warn('Photo access blocked');
          return false;
        }
      case RESULTS.LIMITED:
        return true;
      case RESULTS.GRANTED:
        return true;
      case RESULTS.BLOCKED:
        permissionsRejection();
        return false;
    }
  };

  const getImage = async (isCamera?: boolean) => {
    const permission = await askPermissionForGettingImage(isCamera);
    const {openCamera, openPicker} = ImagePicker;

    if (!permission) {
      return;
    }

    let image;
    if (Platform.OS === 'android') {
      const pickedImage = isCamera
        ? await openCamera({...imagePickerOptions})
        : await openPicker({...imagePickerOptions});

      image = await ImagePicker.openCropper({
        ...imagePickerOptions,
        path: pickedImage.path,
        includeBase64: true,
      });
    } else {
      const imageOptions = {
        ...imagePickerOptions,
        cropping: true,
        maxFiles: 1,
        includeBase64: true,
      };

      image = isCamera
        ? await openCamera(imageOptions)
        : await openPicker(imageOptions);
    }

    const splitedFilename = image.path.split('/');
    const filename = splitedFilename[splitedFilename.length - 1];
    if (filename && image) {
      const photo = {
        source: {uri: `data:${image.mime};base64,${image.data}`},
        data: image,
        filename,
      };
      return photo;
    }
    return null;
  };

  return {getImage};
};
