import {useCallback, useMemo, useState} from 'react';
import {useColorScheme} from 'react-native';
import {ThemeContext} from '../context/ThemeContext';
import {RouteContext} from '../context/RouteContext';
import {ROUTES} from '../constants/routes';

export const useAppContext = () => {
  const isDarkTheme = useColorScheme() === 'dark';
  const {Provider: ThemeProvider} = ThemeContext;
  const {Provider: RouteProvider} = RouteContext;
  const [theme, setTheme] = useState<boolean>(isDarkTheme);
  const [prevRoute, setPrevRoute] = useState<ROUTES>(ROUTES.TAB_ROUTE_MAIN);

  const onThemeToggle = useCallback(() => {
    setTheme(prev => !prev);
  }, []);

  const onRouteChange = useCallback((route: ROUTES) => {
    setPrevRoute(route);
  }, []);

  return useMemo(
    () => ({
      ThemeProvider,
      RouteProvider,
      theme,
      prevRoute,
      onThemeToggle,
      onRouteChange,
      setTheme,
    }),
    [
      RouteProvider,
      ThemeProvider,
      onRouteChange,
      onThemeToggle,
      prevRoute,
      theme,
    ],
  );
};
