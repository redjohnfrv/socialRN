import {MMKVLoader, useMMKVStorage} from 'react-native-mmkv-storage';
import {useCallback, useMemo} from 'react';
import {LOGOUT} from '../constants/common';

const storage = new MMKVLoader().initialize();

export const useToken = () => {
  const [token, setToken] = useMMKVStorage('social-token', storage, '');

  const onSetAuth = useCallback(
    (tkn: string | null) => {
      if (tkn && tkn !== LOGOUT && storage) {
        setToken(tkn);
      } else if (tkn === LOGOUT) {
        setToken('');
      }
    },
    [setToken],
  );

  return useMemo(
    () => ({
      token,
      setToken,
      onSetAuth,
    }),
    [onSetAuth, setToken, token],
  );
};
