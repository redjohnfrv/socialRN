import {useContext} from 'react';
import {colors} from '../assets/styles/colors';
import {ThemeContext} from '../context/ThemeContext';

type ModeColorType = {
  modeColor: typeof colors.dark | typeof colors.light;
  modeColorInverse: typeof colors.dark | typeof colors.light;
};

export const useThemeColors = (): ModeColorType => {
  const {isDarkMode} = useContext(ThemeContext);

  const modeColor = isDarkMode ? colors.dark : colors.light;
  const modeColorInverse = isDarkMode ? colors.light : colors.dark;

  return {
    modeColor,
    modeColorInverse,
  };
};
