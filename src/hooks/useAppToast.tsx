import {useToast} from 'react-native-toast-notifications';
import {useCallback, useMemo} from 'react';

const toastConfig = {
  duration: 4000,
  placement: 'bottom',
  offset: 30,
  animationType: 'slide-in',
} as const;

type ToastType = 'success' | 'danger';

export const useAppToast = () => {
  const toast = useToast();

  const showToast = useCallback(
    (message: string, type: ToastType) => {
      toast.show(message, {
        type: type as string,
        ...toastConfig,
      });
    },
    [toast],
  );

  return useMemo(() => showToast, [showToast]);
};
