import {useContext, useMemo} from 'react';
import {
  GestureResponderEvent,
  StyleProp,
  StyleSheet,
  ViewStyle,
} from 'react-native';
import {useThemeColors} from './useThemeColors';
import {useSwitcher} from './useSwitcher';
import {ThemeContext} from '../context/ThemeContext';
import {ButtonType} from '../ui/DefaultButton/DefaultButton';

export const useButtonStyles = (
  type: ButtonType,
  onPress?: ((event: GestureResponderEvent) => void) | undefined,
  customStyles?: StyleProp<ViewStyle>,
  isDisabled?: boolean,
  isLoading?: boolean,
) => {
  const {modeColor, modeColorInverse} = useThemeColors();
  const {isDarkMode} = useContext(ThemeContext);

  const {
    isOn: isPress,
    on: setIsPressOn,
    off: setIsPressOff,
  } = useSwitcher(false);

  const initialCopyLinkButtonStyles = useMemo(
    () => ({
      backgroundColor: isDarkMode
        ? modeColor.grayscale[200]
        : modeColorInverse.primary.default,
      color: isDarkMode
        ? modeColor.grayscale[700]
        : modeColorInverse.grayscale[700],
    }),
    [
      isDarkMode,
      modeColor.grayscale,
      modeColorInverse.grayscale,
      modeColorInverse.primary.default,
    ],
  );

  const initialButtonStyles = useMemo(
    () => ({
      backgroundColor: isDarkMode
        ? modeColor.grayscale[200]
        : modeColorInverse.primary.default,
      color: isDarkMode
        ? modeColor.primary.default
        : modeColorInverse.grayscale[700],
    }),
    [
      isDarkMode,
      modeColor.grayscale,
      modeColor.primary.default,
      modeColorInverse.grayscale,
      modeColorInverse.primary.default,
    ],
  );

  const pressedButtonStyles = useMemo(
    () => ({
      backgroundColor: isDarkMode
        ? modeColor.primary.default
        : modeColorInverse.grayscale[200],
      color: isDarkMode ? modeColor.grayscale[200] : modeColor.grayscale[100],
    }),
    [
      isDarkMode,
      modeColor.grayscale,
      modeColor.primary.default,
      modeColorInverse.grayscale,
    ],
  );

  const disabledButtonStyles = useMemo(
    () => ({
      backgroundColor: isDarkMode ? modeColor.grayscale[200] : '#CFCFCF',
      color: isDarkMode ? modeColor.grayscale[250] : modeColor.grayscale[300],
    }),
    [isDarkMode, modeColor.grayscale],
  );

  const cs = typeof customStyles === 'object' ? customStyles : undefined;

  const touchProps = useMemo(
    () => ({
      activeOpacity: 1,
      underlayColor: isDisabled
        ? disabledButtonStyles.backgroundColor
        : pressedButtonStyles.backgroundColor,
      style: [
        styles[type],
        {justifyContent: isLoading ? 'center' : 'space-between'},
        {
          backgroundColor: isDisabled
            ? disabledButtonStyles.backgroundColor
            : initialButtonStyles.backgroundColor,
          willChange: 'transform, background-color, color',
          ...cs,
        },
      ],
      onHideUnderlay: () => setIsPressOff(),
      onShowUnderlay: () => setIsPressOn(),
      onPress: (e: GestureResponderEvent) => onPress && onPress(e),
    }),
    [
      cs,
      disabledButtonStyles.backgroundColor,
      initialButtonStyles.backgroundColor,
      isDisabled,
      isLoading,
      onPress,
      pressedButtonStyles.backgroundColor,
      setIsPressOff,
      setIsPressOn,
      type,
    ],
  );

  return useMemo(
    () => ({
      isPress,
      touchProps,
      initialButtonStyles,
      pressedButtonStyles,
      disabledButtonStyles,
      initialCopyLinkButtonStyles,
    }),
    [
      disabledButtonStyles,
      initialButtonStyles,
      initialCopyLinkButtonStyles,
      isPress,
      pressedButtonStyles,
      touchProps,
    ],
  );
};
const styles = StyleSheet.create<Record<ButtonType, any>>({
  large: {
    paddingVertical: 18,
    paddingHorizontal: 36,
    borderRadius: 21,
    alignItems: 'center',
  },
  medium: {
    paddingVertical: 12,
    paddingHorizontal: 24,
    borderRadius: 15,
    alignItems: 'center',
  },
  small: {
    paddingVertical: 12,
    paddingHorizontal: 24,
    borderRadius: 17,
    alignItems: 'center',
    width: 148,
  },
  textIcon: {
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
