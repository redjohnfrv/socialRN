type ArgsType = Array<string | undefined>;

const hasErrorsOnForm = (...args: ArgsType): boolean => {
  const fieldsErrors = args.filter(error => !!error);

  return !!fieldsErrors.length;
};

export {hasErrorsOnForm};
