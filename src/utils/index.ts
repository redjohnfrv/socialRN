import {hasErrorsOnForm} from './returnBoolean';
import {
  getPrettyNumber,
  toCapitalize,
  getValidDate,
  getUploadUrl,
  getS3AvatarUrl,
  getPrettyDate,
  getPrettyAuthorName,
} from './returnString';
import {getColorByState} from './returnVariant';
import {getEyeSecureInputVariant} from './returnVariant';

export const returnVariant = {
  getColorByState,
  getEyeSecureInputVariant,
};

export const returnBoolean = {
  hasErrorsOnForm,
};

export const returnString = {
  toCapitalize,
  getPrettyNumber,
  getValidDate,
  getUploadUrl,
  getS3AvatarUrl,
  getPrettyDate,
  getPrettyAuthorName,
};
