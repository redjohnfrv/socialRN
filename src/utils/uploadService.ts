import axios from 'axios';
import {Image} from 'react-native-image-crop-picker';
import {UploadImageCategory} from '../types';
import {getUploadUrl} from './returnString';

export type ImagePickerData =
  | {source: {uri: string}; data: Image; filename: string}
  | null
  | undefined;

export const getPresignedUrl = async (
  imageName: string,
  token: string,
  category: UploadImageCategory,
) => {
  const configurationObject = {
    method: 'GET',
    url: getUploadUrl(imageName, category),
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  const {data} = await axios(configurationObject);
  return data;
};
