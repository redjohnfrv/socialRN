import {restAPI, s3_signed_URL} from '../constants/api';

const toCapitalize = (value: string): string => {
  if (value) {
    return value[0] + value.slice(1).toLowerCase();
  }

  return value;
};

const getPrettyNumber = (phone: string): string => {
  if (phone.length < 3) {
    return phone;
  }

  if (phone && phone.startsWith('+7')) {
    if (phone.length < 14) {
      const value = phone.toString().replace(/\s/g, '');
      return value.replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    }
  } else {
    return `+7${phone}`;
  }

  return phone;
};

const getPrettyAuthorName = (
  name: string | null | undefined,
  lastName: string | null | undefined,
): string => {
  return `${name} ${lastName ? lastName[0] + '.' : ''}`;
};

const getDoubleNumber = (num: string): string =>
  num.length === 1 ? `0${num}` : num;

const getValidDate = (date: Date): string => {
  const parsedDate = date.toLocaleDateString();

  if (parsedDate.includes('.')) {
    return parsedDate
      .split('.')
      .map(num => getDoubleNumber(num))
      .reverse()
      .join('-');
  }
  if (parsedDate.includes('/')) {
    return parsedDate
      .split('/')
      .map(num => getDoubleNumber(num))
      .reverse()
      .join('-');
  }

  return parsedDate;
};

const getPrettyDate = (date: Date): string => {
  if (date) {
    const parsedDate = date.toLocaleDateString();
    const dateAsArray = parsedDate.split('/');
    const parsedArray = dateAsArray.map((num, index) => {
      if (index === dateAsArray.length - 1) {
        return num.substring(num.length - 2);
      }
      return getDoubleNumber(num);
    });

    return parsedArray.join('.');
  }

  return 'Incorrect date';
};

const getUploadUrl = (fileName: string, category: string): string => {
  return `${restAPI}${s3_signed_URL}?fileName=${fileName}&fileCategory=${category}`;
};

const getS3AvatarUrl = (url: string): string => {
  const urlIndex = url.indexOf('?');
  return url.slice(0, urlIndex);
};

export {
  toCapitalize,
  getPrettyNumber,
  getValidDate,
  getUploadUrl,
  getS3AvatarUrl,
  getPrettyDate,
  getPrettyAuthorName,
};
