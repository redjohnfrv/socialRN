import {colors} from '../assets/styles/colors';
import {EyeVariant} from '../types';

const getColorByState = (
  color: string,
  success: boolean,
  error: string,
): string => {
  if (success) {
    return colors.light.primary.default;
  }

  if (error) {
    return colors.additional.error;
  }

  return color;
};

const getEyeSecureInputVariant = (
  error: boolean,
  success: boolean,
  active: boolean,
): EyeVariant => {
  if (error) {
    return 'error';
  }
  if (success) {
    return 'success';
  }
  if (active) {
    return 'active';
  }

  return 'unable';
};

export {getColorByState, getEyeSecureInputVariant};
