import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import {SafeAreaView, StatusBar, useColorScheme} from 'react-native';
import {ApolloProvider} from '@apollo/client';
import {useClient} from './src/apollo/useClient';
import {useAppContext} from './src/hooks/useAppContext';
import {ToastProvider} from 'react-native-toast-notifications';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {SplashScreen} from './src/screens';

const App = () => {
  const {client} = useClient();
  const isDarkTheme = useColorScheme() === 'dark';
  const {
    ThemeProvider,
    RouteProvider,
    theme,
    prevRoute,
    onRouteChange,
    onThemeToggle,
    setTheme,
  } = useAppContext();

  const backgroundStyles = {
    flex: 1,
    backgroundColor: theme ? Colors.darker : Colors.lighter,
  };

  useEffect(() => {
    setTheme(isDarkTheme);
  }, [isDarkTheme, setTheme]);

  return (
    <ApolloProvider client={client}>
      <ThemeProvider value={{isDarkMode: theme, onThemeToggle}}>
        <RouteProvider value={{prevRoute, onRouteChange}}>
          <ToastProvider>
            <SafeAreaView style={backgroundStyles}>
              <SplashScreen />
              <StatusBar
                translucent
                backgroundColor="transparent"
                barStyle={theme ? 'light-content' : 'dark-content'}
              />
            </SafeAreaView>
          </ToastProvider>
        </RouteProvider>
      </ThemeProvider>
    </ApolloProvider>
  );
};

export default App;
