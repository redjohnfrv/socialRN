## To correctly run this project following next steps:

1. Set JDK version to 11.0
2. Set Ruby version to 2.7.5
3. Run: 
 > npx react-native start
4. Run:
 > npx react-native run-android

***HELPFUL LINKS***

1. [environment setup](https://reactnative.dev/docs/environment-setup)
2. [set up custom fonts RN](https://blog.logrocket.com/adding-custom-fonts-react-native/)
3. [combine navigators](https://dev.to/easybuoy/combining-stack-tab-drawer-navigations-in-react-native-with-react-navigation-5-da)
4. [helpfull with S3 uploading](https://medium.com/geekculture/upload-images-to-aws-s3-using-presigned-url-in-react-native-45059fe3d31b)
5. [share menu doc](https://github.com/react-native-share/react-native-share) / [article](https://blog.logrocket.com/sharing-content-react-native-apps-using-react-native-share/)
6. [configuring deep links](https://reactnavigation.org/docs/configuring-links/)

To generate splash screen image need to execute next steps:

1) Setup Android and IOS files: [instruction in github repo](https://github.com/zoontek/react-native-bootsplash)
2) Then run this command:
> yarn react-native generate-bootsplash src/assets/splash.png \
--background-color=F5FCFF \
--logo-width=280 \
--assets-path=assets \
--flavor=main

================================

To prevent the keyboard from shifting content change AndroidManifest.xml:
> android:windowSoftInputMode="adjustPan"

To prevent force colors change with dark mode on Android go to styles.xml and add:
> `````<item name="android:forceDarkAllowed">false</item>`````

If you need to test deep link navigation you may use this command:
> npx uri-scheme open socialrnapp://favorites --android
